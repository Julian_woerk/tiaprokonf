﻿namespace TIAProKonf.forms
{
    partial class HardwareDialogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quelldateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zieldateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quelldateiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.zieldateiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.generierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ausTIAProKonfExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.gbPaths = new System.Windows.Forms.GroupBox();
            this.txtbox_Version = new System.Windows.Forms.TextBox();
            this.txtbox_DeviceName = new System.Windows.Forms.TextBox();
            this.txtbox_OrderNo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_AddHw = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.label6 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.dgvAttibutes = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dgvRelations = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnImportToTarget = new System.Windows.Forms.Button();
            this.btnAttributes = new System.Windows.Forms.Button();
            this.btnAddDevice = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tvSourceAML = new System.Windows.Forms.TreeView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnTargetExport = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tvTargetAML = new System.Windows.Forms.TreeView();
            this.gbSourcePath = new System.Windows.Forms.GroupBox();
            this.lblSourcePath = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gbTargetPath = new System.Windows.Forms.GroupBox();
            this.lblTargetPath = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.startTIAPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beendeTIAPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verbindeTIAPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sucheProjektToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projektSpeichernToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projektSchließenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStrip1.SuspendLayout();
            this.gbPaths.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttibutes)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRelations)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.gbSourcePath.SuspendLayout();
            this.gbTargetPath.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem,
            this.generierenToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(853, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuToolStripMenuItem,
            this.importierenToolStripMenuItem});
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // neuToolStripMenuItem
            // 
            this.neuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quelldateiToolStripMenuItem,
            this.zieldateiToolStripMenuItem});
            this.neuToolStripMenuItem.Name = "neuToolStripMenuItem";
            this.neuToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.neuToolStripMenuItem.Text = "Neu";
            // 
            // quelldateiToolStripMenuItem
            // 
            this.quelldateiToolStripMenuItem.Name = "quelldateiToolStripMenuItem";
            this.quelldateiToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.quelldateiToolStripMenuItem.Text = "Quelldatei";
            // 
            // zieldateiToolStripMenuItem
            // 
            this.zieldateiToolStripMenuItem.Name = "zieldateiToolStripMenuItem";
            this.zieldateiToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.zieldateiToolStripMenuItem.Text = "Zieldatei";
            // 
            // importierenToolStripMenuItem
            // 
            this.importierenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quelldateiToolStripMenuItem1,
            this.zieldateiToolStripMenuItem1});
            this.importierenToolStripMenuItem.Name = "importierenToolStripMenuItem";
            this.importierenToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.importierenToolStripMenuItem.Text = "importieren";
            // 
            // quelldateiToolStripMenuItem1
            // 
            this.quelldateiToolStripMenuItem1.Name = "quelldateiToolStripMenuItem1";
            this.quelldateiToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.quelldateiToolStripMenuItem1.Text = "Quelldatei";
            // 
            // zieldateiToolStripMenuItem1
            // 
            this.zieldateiToolStripMenuItem1.Name = "zieldateiToolStripMenuItem1";
            this.zieldateiToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.zieldateiToolStripMenuItem1.Text = "Zieldatei";
            // 
            // generierenToolStripMenuItem
            // 
            this.generierenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ausTIAProKonfExcelToolStripMenuItem});
            this.generierenToolStripMenuItem.Name = "generierenToolStripMenuItem";
            this.generierenToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.generierenToolStripMenuItem.Text = "Generieren";
            // 
            // ausTIAProKonfExcelToolStripMenuItem
            // 
            this.ausTIAProKonfExcelToolStripMenuItem.Name = "ausTIAProKonfExcelToolStripMenuItem";
            this.ausTIAProKonfExcelToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.ausTIAProKonfExcelToolStripMenuItem.Text = "aus TIAProKonf-Excel";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startTIAPortalToolStripMenuItem,
            this.beendeTIAPortalToolStripMenuItem,
            this.toolStripSeparator1,
            this.verbindeTIAPortalToolStripMenuItem,
            this.toolStripSeparator2,
            this.sucheProjektToolStripMenuItem,
            this.projektSpeichernToolStripMenuItem,
            this.projektSchließenToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(70, 20);
            this.toolStripMenuItem1.Text = "TIA Portal";
            // 
            // gbPaths
            // 
            this.gbPaths.Controls.Add(this.txtbox_Version);
            this.gbPaths.Controls.Add(this.txtbox_DeviceName);
            this.gbPaths.Controls.Add(this.txtbox_OrderNo);
            this.gbPaths.Controls.Add(this.label7);
            this.gbPaths.Controls.Add(this.label8);
            this.gbPaths.Controls.Add(this.label9);
            this.gbPaths.Controls.Add(this.btn_AddHw);
            this.gbPaths.Controls.Add(this.panel9);
            this.gbPaths.Controls.Add(this.gbSourcePath);
            this.gbPaths.Controls.Add(this.label3);
            this.gbPaths.Controls.Add(this.gbTargetPath);
            this.gbPaths.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbPaths.Location = new System.Drawing.Point(0, 24);
            this.gbPaths.Name = "gbPaths";
            this.gbPaths.Size = new System.Drawing.Size(853, 556);
            this.gbPaths.TabIndex = 10;
            this.gbPaths.TabStop = false;
            this.gbPaths.Text = "AML Pfade";
            // 
            // txtbox_Version
            // 
            this.txtbox_Version.Location = new System.Drawing.Point(345, 450);
            this.txtbox_Version.Name = "txtbox_Version";
            this.txtbox_Version.Size = new System.Drawing.Size(159, 20);
            this.txtbox_Version.TabIndex = 13;
            this.txtbox_Version.Text = "V2.1";
            // 
            // txtbox_DeviceName
            // 
            this.txtbox_DeviceName.Location = new System.Drawing.Point(15, 450);
            this.txtbox_DeviceName.Name = "txtbox_DeviceName";
            this.txtbox_DeviceName.Size = new System.Drawing.Size(160, 20);
            this.txtbox_DeviceName.TabIndex = 11;
            // 
            // txtbox_OrderNo
            // 
            this.txtbox_OrderNo.Location = new System.Drawing.Point(180, 450);
            this.txtbox_OrderNo.Name = "txtbox_OrderNo";
            this.txtbox_OrderNo.Size = new System.Drawing.Size(159, 20);
            this.txtbox_OrderNo.TabIndex = 12;
            this.txtbox_OrderNo.Text = "6ES7 516-3AN01-0AB0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 434);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Type Device name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(177, 434);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Type Order Nr.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(342, 434);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Type Version";
            // 
            // btn_AddHw
            // 
            this.btn_AddHw.Location = new System.Drawing.Point(15, 476);
            this.btn_AddHw.Name = "btn_AddHw";
            this.btn_AddHw.Size = new System.Drawing.Size(88, 20);
            this.btn_AddHw.TabIndex = 17;
            this.btn_AddHw.Text = "Add HW";
            this.btn_AddHw.UseVisualStyleBackColor = true;
            this.btn_AddHw.Click += new System.EventHandler(this.Btn_AddHw_Click);
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.Controls.Add(this.panel2);
            this.panel9.Location = new System.Drawing.Point(0, 89);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(853, 333);
            this.panel9.TabIndex = 10;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.MinimumSize = new System.Drawing.Size(774, 279);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(853, 333);
            this.panel2.TabIndex = 9;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel4);
            this.splitContainer1.Size = new System.Drawing.Size(853, 333);
            this.splitContainer1.SplitterDistance = 260;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.label6);
            this.splitContainer3.Panel1.Controls.Add(this.panel8);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.label4);
            this.splitContainer3.Panel2.Controls.Add(this.panel5);
            this.splitContainer3.Size = new System.Drawing.Size(260, 333);
            this.splitContainer3.SplitterDistance = 172;
            this.splitContainer3.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Attribute";
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.Controls.Add(this.dgvAttibutes);
            this.panel8.Location = new System.Drawing.Point(3, 16);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(254, 156);
            this.panel8.TabIndex = 2;
            // 
            // dgvAttibutes
            // 
            this.dgvAttibutes.AllowUserToAddRows = false;
            this.dgvAttibutes.AllowUserToDeleteRows = false;
            this.dgvAttibutes.AllowUserToResizeRows = false;
            this.dgvAttibutes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttibutes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAttibutes.Location = new System.Drawing.Point(0, 0);
            this.dgvAttibutes.MultiSelect = false;
            this.dgvAttibutes.Name = "dgvAttibutes";
            this.dgvAttibutes.Size = new System.Drawing.Size(254, 156);
            this.dgvAttibutes.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Verbindungen";
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.dgvRelations);
            this.panel5.Location = new System.Drawing.Point(3, 16);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(254, 141);
            this.panel5.TabIndex = 1;
            // 
            // dgvRelations
            // 
            this.dgvRelations.AllowUserToAddRows = false;
            this.dgvRelations.AllowUserToDeleteRows = false;
            this.dgvRelations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRelations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRelations.Location = new System.Drawing.Point(0, 0);
            this.dgvRelations.Name = "dgvRelations";
            this.dgvRelations.Size = new System.Drawing.Size(254, 141);
            this.dgvRelations.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.splitContainer2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(589, 333);
            this.panel4.TabIndex = 3;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tableLayoutPanel3);
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            this.splitContainer2.Panel1.Controls.Add(this.panel7);
            this.splitContainer2.Panel1MinSize = 247;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer2.Panel2.Controls.Add(this.label5);
            this.splitContainer2.Panel2.Controls.Add(this.panel6);
            this.splitContainer2.Panel2MinSize = 247;
            this.splitContainer2.Size = new System.Drawing.Size(589, 333);
            this.splitContainer2.SplitterDistance = 278;
            this.splitContainer2.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.btnImportToTarget, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnAttributes, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnAddDevice, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel3.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 306);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(278, 27);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // btnImportToTarget
            // 
            this.btnImportToTarget.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnImportToTarget.Location = new System.Drawing.Point(200, 3);
            this.btnImportToTarget.Name = "btnImportToTarget";
            this.btnImportToTarget.Size = new System.Drawing.Size(75, 21);
            this.btnImportToTarget.TabIndex = 7;
            this.btnImportToTarget.Text = "→";
            this.btnImportToTarget.UseVisualStyleBackColor = true;
            // 
            // btnAttributes
            // 
            this.btnAttributes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAttributes.Location = new System.Drawing.Point(84, 3);
            this.btnAttributes.Name = "btnAttributes";
            this.btnAttributes.Size = new System.Drawing.Size(75, 21);
            this.btnAttributes.TabIndex = 5;
            this.btnAttributes.Text = "Attribute...";
            this.btnAttributes.UseVisualStyleBackColor = true;
            // 
            // btnAddDevice
            // 
            this.btnAddDevice.Location = new System.Drawing.Point(3, 3);
            this.btnAddDevice.Name = "btnAddDevice";
            this.btnAddDevice.Size = new System.Drawing.Size(75, 21);
            this.btnAddDevice.TabIndex = 4;
            this.btnAddDevice.Text = "Hinzufügen";
            this.btnAddDevice.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Quelle";
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.Controls.Add(this.tvSourceAML);
            this.panel7.Location = new System.Drawing.Point(0, 16);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(277, 287);
            this.panel7.TabIndex = 1;
            // 
            // tvSourceAML
            // 
            this.tvSourceAML.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvSourceAML.Location = new System.Drawing.Point(0, 0);
            this.tvSourceAML.Name = "tvSourceAML";
            this.tvSourceAML.Size = new System.Drawing.Size(277, 287);
            this.tvSourceAML.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.btnTargetExport, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 306);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(307, 27);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // btnTargetExport
            // 
            this.btnTargetExport.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnTargetExport.Location = new System.Drawing.Point(215, 3);
            this.btnTargetExport.Name = "btnTargetExport";
            this.btnTargetExport.Size = new System.Drawing.Size(89, 21);
            this.btnTargetExport.TabIndex = 8;
            this.btnTargetExport.Text = "Ziel exportieren";
            this.btnTargetExport.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Ziel";
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.Controls.Add(this.tvTargetAML);
            this.panel6.Location = new System.Drawing.Point(2, 16);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(305, 287);
            this.panel6.TabIndex = 0;
            // 
            // tvTargetAML
            // 
            this.tvTargetAML.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvTargetAML.Location = new System.Drawing.Point(0, 0);
            this.tvTargetAML.Name = "tvTargetAML";
            this.tvTargetAML.Size = new System.Drawing.Size(305, 287);
            this.tvTargetAML.TabIndex = 1;
            // 
            // gbSourcePath
            // 
            this.gbSourcePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSourcePath.Controls.Add(this.lblSourcePath);
            this.gbSourcePath.Controls.Add(this.label2);
            this.gbSourcePath.Location = new System.Drawing.Point(6, 19);
            this.gbSourcePath.Name = "gbSourcePath";
            this.gbSourcePath.Size = new System.Drawing.Size(841, 29);
            this.gbSourcePath.TabIndex = 6;
            this.gbSourcePath.TabStop = false;
            this.gbSourcePath.Text = "Quelle";
            // 
            // lblSourcePath
            // 
            this.lblSourcePath.AccessibleName = "";
            this.lblSourcePath.AutoSize = true;
            this.lblSourcePath.Location = new System.Drawing.Point(6, 13);
            this.lblSourcePath.Name = "lblSourcePath";
            this.lblSourcePath.Size = new System.Drawing.Size(0, 13);
            this.lblSourcePath.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AccessibleName = "label2";
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AccessibleName = "label2";
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 3;
            // 
            // gbTargetPath
            // 
            this.gbTargetPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTargetPath.Controls.Add(this.lblTargetPath);
            this.gbTargetPath.Location = new System.Drawing.Point(6, 54);
            this.gbTargetPath.Name = "gbTargetPath";
            this.gbTargetPath.Size = new System.Drawing.Size(841, 29);
            this.gbTargetPath.TabIndex = 7;
            this.gbTargetPath.TabStop = false;
            this.gbTargetPath.Text = "Ziel";
            // 
            // lblTargetPath
            // 
            this.lblTargetPath.AccessibleName = "label2";
            this.lblTargetPath.AutoSize = true;
            this.lblTargetPath.Location = new System.Drawing.Point(6, 13);
            this.lblTargetPath.Name = "lblTargetPath";
            this.lblTargetPath.Size = new System.Drawing.Size(0, 13);
            this.lblTargetPath.TabIndex = 3;
            // 
            // startTIAPortalToolStripMenuItem
            // 
            this.startTIAPortalToolStripMenuItem.Name = "startTIAPortalToolStripMenuItem";
            this.startTIAPortalToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.startTIAPortalToolStripMenuItem.Text = "Start TIA Portal";
            // 
            // beendeTIAPortalToolStripMenuItem
            // 
            this.beendeTIAPortalToolStripMenuItem.Name = "beendeTIAPortalToolStripMenuItem";
            this.beendeTIAPortalToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.beendeTIAPortalToolStripMenuItem.Text = "Beende TIA Portal";
            // 
            // verbindeTIAPortalToolStripMenuItem
            // 
            this.verbindeTIAPortalToolStripMenuItem.Name = "verbindeTIAPortalToolStripMenuItem";
            this.verbindeTIAPortalToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.verbindeTIAPortalToolStripMenuItem.Text = "Verbinde TIA Portal";
            // 
            // sucheProjektToolStripMenuItem
            // 
            this.sucheProjektToolStripMenuItem.Name = "sucheProjektToolStripMenuItem";
            this.sucheProjektToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sucheProjektToolStripMenuItem.Text = "Projekt öffnen";
            // 
            // projektSpeichernToolStripMenuItem
            // 
            this.projektSpeichernToolStripMenuItem.Name = "projektSpeichernToolStripMenuItem";
            this.projektSpeichernToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.projektSpeichernToolStripMenuItem.Text = "Projekt speichern";
            // 
            // projektSchließenToolStripMenuItem
            // 
            this.projektSchließenToolStripMenuItem.Name = "projektSchließenToolStripMenuItem";
            this.projektSchließenToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.projektSchließenToolStripMenuItem.Text = "Projekt schließen";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // HardwareDialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 580);
            this.Controls.Add(this.gbPaths);
            this.Controls.Add(this.menuStrip1);
            this.Name = "HardwareDialogForm";
            this.Text = "Hardware";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.gbPaths.ResumeLayout(false);
            this.gbPaths.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttibutes)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRelations)).EndInit();
            this.panel4.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.gbSourcePath.ResumeLayout(false);
            this.gbSourcePath.PerformLayout();
            this.gbTargetPath.ResumeLayout(false);
            this.gbTargetPath.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem neuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quelldateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zieldateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quelldateiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem zieldateiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem generierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ausTIAProKonfExcelToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbPaths;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.DataGridView dgvAttibutes;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataGridView dgvRelations;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnImportToTarget;
        private System.Windows.Forms.Button btnAttributes;
        private System.Windows.Forms.Button btnAddDevice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TreeView tvSourceAML;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnTargetExport;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TreeView tvTargetAML;
        private System.Windows.Forms.GroupBox gbSourcePath;
        private System.Windows.Forms.Label lblSourcePath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gbTargetPath;
        private System.Windows.Forms.Label lblTargetPath;
        public System.Windows.Forms.TextBox txtbox_Version;
        public System.Windows.Forms.TextBox txtbox_DeviceName;
        public System.Windows.Forms.TextBox txtbox_OrderNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_AddHw;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem startTIAPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beendeTIAPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem verbindeTIAPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem sucheProjektToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projektSpeichernToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projektSchließenToolStripMenuItem;
    }
}