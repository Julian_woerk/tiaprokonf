﻿namespace TIAProKonf.forms
{
    partial class OrdnerstrukturDialogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.OrdnerstrukturDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.öffnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excelToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.externeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aMLToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tIAPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startTiaPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beendeTiaPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.verbindeTiaPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.sucheProjektToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speichernToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.schließeProjektToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtbox_Excel_Datenbank_Ordnerstruktur = new System.Windows.Forms.TextBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.neueAnlageHinzufügenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.löschenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button_add_Anlage = new System.Windows.Forms.Button();
            this.button_NodeDelete = new System.Windows.Forms.Button();
            this.button_DeleteAll = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_Parent = new System.Windows.Forms.TextBox();
            this.textBox_Child = new System.Windows.Forms.TextBox();
            this.textBox_Grandchild = new System.Windows.Forms.TextBox();
            this.button_AddNodeParent = new System.Windows.Forms.Button();
            this.button_AddNodeChild = new System.Windows.Forms.Button();
            this.button_AddNodeGrandchild = new System.Windows.Forms.Button();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.neuenBehälterHinzufügenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.umbennenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.löschenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_ExcelImport = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // OrdnerstrukturDialog
            // 
            this.OrdnerstrukturDialog.FileName = "openFileDialog1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem,
            this.tIAPortalToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(626, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.öffnenToolStripMenuItem});
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // öffnenToolStripMenuItem
            // 
            this.öffnenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.excelToolStripMenuItem1,
            this.aMLToolStripMenuItem1});
            this.öffnenToolStripMenuItem.Name = "öffnenToolStripMenuItem";
            this.öffnenToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.öffnenToolStripMenuItem.Text = "Öffnen";
            // 
            // excelToolStripMenuItem1
            // 
            this.excelToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.externeToolStripMenuItem,
            this.openExcelToolStripMenuItem});
            this.excelToolStripMenuItem1.Name = "excelToolStripMenuItem1";
            this.excelToolStripMenuItem1.Size = new System.Drawing.Size(101, 22);
            this.excelToolStripMenuItem1.Text = "Excel";
            // 
            // externeToolStripMenuItem
            // 
            this.externeToolStripMenuItem.Name = "externeToolStripMenuItem";
            this.externeToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.externeToolStripMenuItem.Text = "Externe Quelle ";
            // 
            // openExcelToolStripMenuItem
            // 
            this.openExcelToolStripMenuItem.Name = "openExcelToolStripMenuItem";
            this.openExcelToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.openExcelToolStripMenuItem.Text = "Open Excel-Datenbank";
            // 
            // aMLToolStripMenuItem1
            // 
            this.aMLToolStripMenuItem1.Name = "aMLToolStripMenuItem1";
            this.aMLToolStripMenuItem1.Size = new System.Drawing.Size(101, 22);
            this.aMLToolStripMenuItem1.Text = "AML";
            // 
            // tIAPortalToolStripMenuItem
            // 
            this.tIAPortalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startTiaPortalToolStripMenuItem,
            this.beendeTiaPortalToolStripMenuItem,
            this.toolStripSeparator1,
            this.verbindeTiaPortalToolStripMenuItem,
            this.toolStripSeparator2,
            this.sucheProjektToolStripMenuItem,
            this.speichernToolStripMenuItem,
            this.schließeProjektToolStripMenuItem});
            this.tIAPortalToolStripMenuItem.Name = "tIAPortalToolStripMenuItem";
            this.tIAPortalToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.tIAPortalToolStripMenuItem.Text = "Tia Portal";
            // 
            // startTiaPortalToolStripMenuItem
            // 
            this.startTiaPortalToolStripMenuItem.Enabled = false;
            this.startTiaPortalToolStripMenuItem.Name = "startTiaPortalToolStripMenuItem";
            this.startTiaPortalToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.startTiaPortalToolStripMenuItem.Text = "Start TIA Portal";
            // 
            // beendeTiaPortalToolStripMenuItem
            // 
            this.beendeTiaPortalToolStripMenuItem.Name = "beendeTiaPortalToolStripMenuItem";
            this.beendeTiaPortalToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.beendeTiaPortalToolStripMenuItem.Text = "Beende TIA Portal";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(171, 6);
            // 
            // verbindeTiaPortalToolStripMenuItem
            // 
            this.verbindeTiaPortalToolStripMenuItem.Name = "verbindeTiaPortalToolStripMenuItem";
            this.verbindeTiaPortalToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.verbindeTiaPortalToolStripMenuItem.Text = "Verbinde TIA Portal";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(171, 6);
            // 
            // sucheProjektToolStripMenuItem
            // 
            this.sucheProjektToolStripMenuItem.Name = "sucheProjektToolStripMenuItem";
            this.sucheProjektToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.sucheProjektToolStripMenuItem.Text = "Projekt öffnen";
            // 
            // speichernToolStripMenuItem
            // 
            this.speichernToolStripMenuItem.Name = "speichernToolStripMenuItem";
            this.speichernToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.speichernToolStripMenuItem.Text = "Projekt speichern";
            // 
            // schließeProjektToolStripMenuItem
            // 
            this.schließeProjektToolStripMenuItem.Name = "schließeProjektToolStripMenuItem";
            this.schließeProjektToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.schließeProjektToolStripMenuItem.Text = "Projekt schließen";
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 291);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(602, 95);
            this.listBox1.TabIndex = 30;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 274);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "Statusanzeige";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Excel-Datenbank";
            // 
            // txtbox_Excel_Datenbank_Ordnerstruktur
            // 
            this.txtbox_Excel_Datenbank_Ordnerstruktur.Location = new System.Drawing.Point(101, 27);
            this.txtbox_Excel_Datenbank_Ordnerstruktur.Name = "txtbox_Excel_Datenbank_Ordnerstruktur";
            this.txtbox_Excel_Datenbank_Ordnerstruktur.Size = new System.Drawing.Size(513, 20);
            this.txtbox_Excel_Datenbank_Ordnerstruktur.TabIndex = 32;
            // 
            // treeView1
            // 
            this.treeView1.ContextMenuStrip = this.contextMenuStrip1;
            this.treeView1.Location = new System.Drawing.Point(13, 52);
            this.treeView1.Margin = new System.Windows.Forms.Padding(2);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(305, 220);
            this.treeView1.TabIndex = 34;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neueAnlageHinzufügenToolStripMenuItem,
            this.löschenToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(206, 48);
            // 
            // neueAnlageHinzufügenToolStripMenuItem
            // 
            this.neueAnlageHinzufügenToolStripMenuItem.Name = "neueAnlageHinzufügenToolStripMenuItem";
            this.neueAnlageHinzufügenToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.neueAnlageHinzufügenToolStripMenuItem.Text = "Neue Anlage hinzufügen";
            // 
            // löschenToolStripMenuItem
            // 
            this.löschenToolStripMenuItem.Name = "löschenToolStripMenuItem";
            this.löschenToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.löschenToolStripMenuItem.Text = "Löschen";
            // 
            // button_add_Anlage
            // 
            this.button_add_Anlage.Location = new System.Drawing.Point(325, 52);
            this.button_add_Anlage.Margin = new System.Windows.Forms.Padding(2);
            this.button_add_Anlage.Name = "button_add_Anlage";
            this.button_add_Anlage.Size = new System.Drawing.Size(140, 23);
            this.button_add_Anlage.TabIndex = 35;
            this.button_add_Anlage.Text = "Export aus TIA Portal";
            this.button_add_Anlage.UseVisualStyleBackColor = true;
            this.button_add_Anlage.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button_NodeDelete
            // 
            this.button_NodeDelete.Location = new System.Drawing.Point(377, 253);
            this.button_NodeDelete.Margin = new System.Windows.Forms.Padding(2);
            this.button_NodeDelete.Name = "button_NodeDelete";
            this.button_NodeDelete.Size = new System.Drawing.Size(99, 26);
            this.button_NodeDelete.TabIndex = 36;
            this.button_NodeDelete.Text = "Knoten löschen";
            this.button_NodeDelete.UseVisualStyleBackColor = true;
            this.button_NodeDelete.Click += new System.EventHandler(this.Button_NodeDelete_Click);
            // 
            // button_DeleteAll
            // 
            this.button_DeleteAll.Location = new System.Drawing.Point(480, 253);
            this.button_DeleteAll.Margin = new System.Windows.Forms.Padding(2);
            this.button_DeleteAll.Name = "button_DeleteAll";
            this.button_DeleteAll.Size = new System.Drawing.Size(99, 26);
            this.button_DeleteAll.TabIndex = 37;
            this.button_DeleteAll.Text = "Alles löschen";
            this.button_DeleteAll.UseVisualStyleBackColor = true;
            this.button_DeleteAll.Click += new System.EventHandler(this.Button_DeleteAll_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(322, 169);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "Anlage";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(322, 200);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "Behälter";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(322, 229);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 40;
            this.label4.Text = "Ventil";
            // 
            // textBox_Parent
            // 
            this.textBox_Parent.Location = new System.Drawing.Point(377, 162);
            this.textBox_Parent.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_Parent.Name = "textBox_Parent";
            this.textBox_Parent.Size = new System.Drawing.Size(157, 20);
            this.textBox_Parent.TabIndex = 41;
            // 
            // textBox_Child
            // 
            this.textBox_Child.Location = new System.Drawing.Point(377, 191);
            this.textBox_Child.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_Child.Name = "textBox_Child";
            this.textBox_Child.Size = new System.Drawing.Size(157, 20);
            this.textBox_Child.TabIndex = 42;
            // 
            // textBox_Grandchild
            // 
            this.textBox_Grandchild.Location = new System.Drawing.Point(377, 220);
            this.textBox_Grandchild.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_Grandchild.Name = "textBox_Grandchild";
            this.textBox_Grandchild.Size = new System.Drawing.Size(157, 20);
            this.textBox_Grandchild.TabIndex = 43;
            // 
            // button_AddNodeParent
            // 
            this.button_AddNodeParent.Location = new System.Drawing.Point(538, 162);
            this.button_AddNodeParent.Margin = new System.Windows.Forms.Padding(2);
            this.button_AddNodeParent.Name = "button_AddNodeParent";
            this.button_AddNodeParent.Size = new System.Drawing.Size(75, 23);
            this.button_AddNodeParent.TabIndex = 44;
            this.button_AddNodeParent.Text = "Hinzufügen";
            this.button_AddNodeParent.UseVisualStyleBackColor = true;
            this.button_AddNodeParent.Click += new System.EventHandler(this.Button_AddNodeParent_Click);
            // 
            // button_AddNodeChild
            // 
            this.button_AddNodeChild.Location = new System.Drawing.Point(538, 191);
            this.button_AddNodeChild.Margin = new System.Windows.Forms.Padding(2);
            this.button_AddNodeChild.Name = "button_AddNodeChild";
            this.button_AddNodeChild.Size = new System.Drawing.Size(75, 23);
            this.button_AddNodeChild.TabIndex = 45;
            this.button_AddNodeChild.Text = "Hinzufügen";
            this.button_AddNodeChild.UseVisualStyleBackColor = true;
            this.button_AddNodeChild.Click += new System.EventHandler(this.Button_AddNodeChild_Click);
            // 
            // button_AddNodeGrandchild
            // 
            this.button_AddNodeGrandchild.Location = new System.Drawing.Point(538, 220);
            this.button_AddNodeGrandchild.Margin = new System.Windows.Forms.Padding(2);
            this.button_AddNodeGrandchild.Name = "button_AddNodeGrandchild";
            this.button_AddNodeGrandchild.Size = new System.Drawing.Size(75, 23);
            this.button_AddNodeGrandchild.TabIndex = 46;
            this.button_AddNodeGrandchild.Text = "Hinzufügen";
            this.button_AddNodeGrandchild.UseVisualStyleBackColor = true;
            this.button_AddNodeGrandchild.Click += new System.EventHandler(this.Button_AddNodeGrandchild_Click);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuenBehälterHinzufügenToolStripMenuItem,
            this.umbennenToolStripMenuItem,
            this.löschenToolStripMenuItem1});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(219, 70);
            // 
            // neuenBehälterHinzufügenToolStripMenuItem
            // 
            this.neuenBehälterHinzufügenToolStripMenuItem.Name = "neuenBehälterHinzufügenToolStripMenuItem";
            this.neuenBehälterHinzufügenToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.neuenBehälterHinzufügenToolStripMenuItem.Text = "Neuen Behälter hinzufügen";
            // 
            // umbennenToolStripMenuItem
            // 
            this.umbennenToolStripMenuItem.Name = "umbennenToolStripMenuItem";
            this.umbennenToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.umbennenToolStripMenuItem.Text = "Umbennen";
            // 
            // löschenToolStripMenuItem1
            // 
            this.löschenToolStripMenuItem1.Name = "löschenToolStripMenuItem1";
            this.löschenToolStripMenuItem1.Size = new System.Drawing.Size(218, 22);
            this.löschenToolStripMenuItem1.Text = "Löschen";
            // 
            // btn_ExcelImport
            // 
            this.btn_ExcelImport.Location = new System.Drawing.Point(325, 80);
            this.btn_ExcelImport.Name = "btn_ExcelImport";
            this.btn_ExcelImport.Size = new System.Drawing.Size(140, 23);
            this.btn_ExcelImport.TabIndex = 48;
            this.btn_ExcelImport.Text = "Import nach TIA Portal";
            this.btn_ExcelImport.UseVisualStyleBackColor = true;
            // 
            // OrdnerstrukturDialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 396);
            this.Controls.Add(this.btn_ExcelImport);
            this.Controls.Add(this.button_AddNodeGrandchild);
            this.Controls.Add(this.button_AddNodeChild);
            this.Controls.Add(this.button_AddNodeParent);
            this.Controls.Add(this.textBox_Grandchild);
            this.Controls.Add(this.textBox_Child);
            this.Controls.Add(this.textBox_Parent);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_DeleteAll);
            this.Controls.Add(this.button_NodeDelete);
            this.Controls.Add(this.button_add_Anlage);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtbox_Excel_Datenbank_Ordnerstruktur);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "OrdnerstrukturDialogForm";
            this.Text = "Ordnerstruktur";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog OrdnerstrukturDialog;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem öffnenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem excelToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem externeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aMLToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tIAPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startTiaPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beendeTiaPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verbindeTiaPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sucheProjektToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem speichernToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem schließeProjektToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtbox_Excel_Datenbank_Ordnerstruktur;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button button_add_Anlage;
        private System.Windows.Forms.Button button_NodeDelete;
        private System.Windows.Forms.Button button_DeleteAll;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_Parent;
        private System.Windows.Forms.TextBox textBox_Child;
        private System.Windows.Forms.TextBox textBox_Grandchild;
        private System.Windows.Forms.Button button_AddNodeParent;
        private System.Windows.Forms.Button button_AddNodeChild;
        private System.Windows.Forms.Button button_AddNodeGrandchild;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem neueAnlageHinzufügenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem löschenToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem neuenBehälterHinzufügenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem umbennenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem löschenToolStripMenuItem1;
        private System.Windows.Forms.Button btn_ExcelImport;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}