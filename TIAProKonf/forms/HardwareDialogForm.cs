﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TIAProKonf.Generals;
using TIAProKonf.Database;
using TIAProKonf.ImportFile;
using TIAProKonf.WorkOnTia;
using ExcelDataReader;
using System.IO;
using TIAProKonf.forms;



namespace TIAProKonf.forms
{
    public partial class HardwareDialogForm : Form
    {

        private TiaOpennessAccess _tiaOpennessAccess2;
        private WorkOnDatabase _workOnDatabase2;

        public HardwareDialogForm()
        {
            InitializeComponent();
            _tiaOpennessAccess2 = new TiaOpennessAccess();
            _workOnDatabase2 = new WorkOnDatabase();
        }

        private void Btn_AddHw_Click(object sender, EventArgs e)
        {
            this.timer1.Start();
            UserContainer.DeviceNameGUI = txtbox_DeviceName.Text;
            UserContainer.OrderNoGUI = txtbox_OrderNo.Text;
            UserContainer.VersionNoGUI = txtbox_Version.Text;
            _tiaOpennessAccess2.AddHW();
            this.timer1.Stop();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(UserContainer.MyExcel.readCell(0, 0));
        }

    }
}
