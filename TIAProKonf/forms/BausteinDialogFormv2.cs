﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TIAProKonf.forms
{
    public partial class BausteinDialogFormv2 : Form
    {
        public BausteinDialogFormv2()
        {
            InitializeComponent();
            treeView_Bausteinauswahl.Nodes.Add("A");
            treeView_Bausteinauswahl.Nodes.Add("B");
            treeView_Bausteinauswahl.Nodes.Add("Bibliotheksbausteine");

            treeView_Bausteinauswahl.Nodes[0].Nodes.Add("B001");
            treeView_Bausteinauswahl.Nodes[0].Nodes.Add("B002");
            treeView_Bausteinauswahl.Nodes[1].Nodes.Add("B003");

            treeView_Bausteinauswahl.Nodes[0].Nodes[0].Nodes.Add("Analog");
            treeView_Bausteinauswahl.Nodes[0].Nodes[0].Nodes.Add("Ventile");

            treeView_Bausteinauswahl.Nodes[0].Nodes[1].Nodes.Add("Analog");
            treeView_Bausteinauswahl.Nodes[0].Nodes[1].Nodes.Add("Ventile");

            treeView_Bausteinauswahl.Nodes[1].Nodes[0].Nodes.Add("Analog");
            treeView_Bausteinauswahl.Nodes[1].Nodes[0].Nodes.Add("Ventile");

            treeView_Bausteinauswahl.Nodes[0].Nodes[0].Nodes[1].Nodes.Add("V001");

            treeView_Bausteinauswahl.ExpandAll();


            comboBox3.SelectedIndex = 1;

            comboBox5.SelectedIndex = 1;
            comboBox6.SelectedIndex = 1;
            comboBox7.SelectedIndex = 1;
            comboBox8.SelectedIndex = 1;

            comboBox12.SelectedIndex = 1;
            comboBox13.SelectedIndex = 1;
            comboBox14.SelectedIndex = 1;
            comboBox15.SelectedIndex = 1;
            comboBox16.SelectedIndex = 1;
            comboBox17.SelectedIndex = 1;
            comboBox18.SelectedIndex = 1;
            comboBox19.SelectedIndex = 1;
            comboBox20.SelectedIndex = 1;
            comboBox21.SelectedIndex = 1;
            comboBox26.SelectedIndex = 1;
            comboBox27.SelectedIndex = 1;
            comboBox28.SelectedIndex = 1;
            comboBox29.SelectedIndex = 1;
            comboBox122.SelectedIndex = 1;
            comboBox123.SelectedIndex = 1;
            comboBox124.SelectedIndex = 1;
            comboBox125.SelectedIndex = 1;






        }

        private void Label4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Zykluszeit des Bausteinaufrufs in Sec.", label1);
        }
    }
}
