﻿namespace TIAProKonf.forms
{
    partial class VariablentabellenDialogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_Extern = new System.Windows.Forms.DataGridView();
            this.dataGridView_TIAProKonf = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_Sheet_extern = new System.Windows.Forms.ComboBox();
            this.cboSheet = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.öffnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excelToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.openExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.externeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aMLToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.speichernToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speichernUnterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.druckenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tIAPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startTIAPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beendeTIAPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.verbindeTIAPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.sucheProjektToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projektspeicherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projektSchließenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbox_VarTab_Pfad_ex = new System.Windows.Forms.TextBox();
            this.textBox_VarTab_pfad = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Extern)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_TIAProKonf)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView_Extern
            // 
            this.dataGridView_Extern.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Extern.Location = new System.Drawing.Point(12, 53);
            this.dataGridView_Extern.Name = "dataGridView_Extern";
            this.dataGridView_Extern.Size = new System.Drawing.Size(556, 728);
            this.dataGridView_Extern.TabIndex = 19;
            // 
            // dataGridView_TIAProKonf
            // 
            this.dataGridView_TIAProKonf.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_TIAProKonf.Location = new System.Drawing.Point(634, 54);
            this.dataGridView_TIAProKonf.Name = "dataGridView_TIAProKonf";
            this.dataGridView_TIAProKonf.Size = new System.Drawing.Size(556, 728);
            this.dataGridView_TIAProKonf.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(632, 787);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Arbeitsblatt";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 790);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Arbeitsblatt";
            // 
            // comboBox_Sheet_extern
            // 
            this.comboBox_Sheet_extern.FormattingEnabled = true;
            this.comboBox_Sheet_extern.Location = new System.Drawing.Point(74, 787);
            this.comboBox_Sheet_extern.Name = "comboBox_Sheet_extern";
            this.comboBox_Sheet_extern.Size = new System.Drawing.Size(121, 21);
            this.comboBox_Sheet_extern.TabIndex = 23;
            this.comboBox_Sheet_extern.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Sheet_extern_SelectedIndexChanged);
            // 
            // cboSheet
            // 
            this.cboSheet.FormattingEnabled = true;
            this.cboSheet.Location = new System.Drawing.Point(697, 785);
            this.cboSheet.Name = "cboSheet";
            this.cboSheet.Size = new System.Drawing.Size(135, 21);
            this.cboSheet.TabIndex = 22;
            this.cboSheet.SelectedIndexChanged += new System.EventHandler(this.CboSheet_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem,
            this.tIAPortalToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1204, 24);
            this.menuStrip1.TabIndex = 26;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.öffnenToolStripMenuItem,
            this.toolStripSeparator2,
            this.speichernToolStripMenuItem,
            this.speichernUnterToolStripMenuItem,
            this.toolStripSeparator1,
            this.druckenToolStripMenuItem,
            this.exportierenToolStripMenuItem,
            this.importierenToolStripMenuItem});
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // öffnenToolStripMenuItem
            // 
            this.öffnenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.excelToolStripMenuItem1,
            this.aMLToolStripMenuItem1});
            this.öffnenToolStripMenuItem.Name = "öffnenToolStripMenuItem";
            this.öffnenToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.öffnenToolStripMenuItem.Text = "Öffnen";
            // 
            // excelToolStripMenuItem1
            // 
            this.excelToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openExcelToolStripMenuItem,
            this.externeToolStripMenuItem});
            this.excelToolStripMenuItem1.Name = "excelToolStripMenuItem1";
            this.excelToolStripMenuItem1.Size = new System.Drawing.Size(101, 22);
            this.excelToolStripMenuItem1.Text = "Excel";
            // 
            // openExcelToolStripMenuItem
            // 
            this.openExcelToolStripMenuItem.Name = "openExcelToolStripMenuItem";
            this.openExcelToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.openExcelToolStripMenuItem.Text = "Open Excel-Datenbank";
            this.openExcelToolStripMenuItem.Click += new System.EventHandler(this.OpenExcelToolStripMenuItem_Click);
            // 
            // externeToolStripMenuItem
            // 
            this.externeToolStripMenuItem.Name = "externeToolStripMenuItem";
            this.externeToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.externeToolStripMenuItem.Text = "Externe Quelle ";
            this.externeToolStripMenuItem.Click += new System.EventHandler(this.ExterneToolStripMenuItem_Click);
            // 
            // aMLToolStripMenuItem1
            // 
            this.aMLToolStripMenuItem1.Name = "aMLToolStripMenuItem1";
            this.aMLToolStripMenuItem1.Size = new System.Drawing.Size(101, 22);
            this.aMLToolStripMenuItem1.Text = "AML";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(154, 6);
            // 
            // speichernToolStripMenuItem
            // 
            this.speichernToolStripMenuItem.Name = "speichernToolStripMenuItem";
            this.speichernToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.speichernToolStripMenuItem.Text = "Speichern ";
            // 
            // speichernUnterToolStripMenuItem
            // 
            this.speichernUnterToolStripMenuItem.Name = "speichernUnterToolStripMenuItem";
            this.speichernUnterToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.speichernUnterToolStripMenuItem.Text = "Speichern unter";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(154, 6);
            // 
            // druckenToolStripMenuItem
            // 
            this.druckenToolStripMenuItem.Enabled = false;
            this.druckenToolStripMenuItem.Name = "druckenToolStripMenuItem";
            this.druckenToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.druckenToolStripMenuItem.Text = "Drucken";
            // 
            // exportierenToolStripMenuItem
            // 
            this.exportierenToolStripMenuItem.Name = "exportierenToolStripMenuItem";
            this.exportierenToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.exportierenToolStripMenuItem.Text = "Exportieren";
            // 
            // importierenToolStripMenuItem
            // 
            this.importierenToolStripMenuItem.Name = "importierenToolStripMenuItem";
            this.importierenToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.importierenToolStripMenuItem.Text = "Importieren";
            // 
            // tIAPortalToolStripMenuItem
            // 
            this.tIAPortalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startTIAPortalToolStripMenuItem,
            this.beendeTIAPortalToolStripMenuItem,
            this.toolStripSeparator3,
            this.verbindeTIAPortalToolStripMenuItem,
            this.toolStripSeparator4,
            this.sucheProjektToolStripMenuItem,
            this.projektspeicherToolStripMenuItem,
            this.projektSchließenToolStripMenuItem});
            this.tIAPortalToolStripMenuItem.Name = "tIAPortalToolStripMenuItem";
            this.tIAPortalToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.tIAPortalToolStripMenuItem.Text = "TIA Portal";
            // 
            // startTIAPortalToolStripMenuItem
            // 
            this.startTIAPortalToolStripMenuItem.Name = "startTIAPortalToolStripMenuItem";
            this.startTIAPortalToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.startTIAPortalToolStripMenuItem.Text = "Start TIA Portal";
            // 
            // beendeTIAPortalToolStripMenuItem
            // 
            this.beendeTIAPortalToolStripMenuItem.Name = "beendeTIAPortalToolStripMenuItem";
            this.beendeTIAPortalToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.beendeTIAPortalToolStripMenuItem.Text = "Beende TIA Portal";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(171, 6);
            // 
            // verbindeTIAPortalToolStripMenuItem
            // 
            this.verbindeTIAPortalToolStripMenuItem.Name = "verbindeTIAPortalToolStripMenuItem";
            this.verbindeTIAPortalToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.verbindeTIAPortalToolStripMenuItem.Text = "Verbinde TIA Portal";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(171, 6);
            // 
            // sucheProjektToolStripMenuItem
            // 
            this.sucheProjektToolStripMenuItem.Name = "sucheProjektToolStripMenuItem";
            this.sucheProjektToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.sucheProjektToolStripMenuItem.Text = "Projekt öffnen";
            // 
            // projektspeicherToolStripMenuItem
            // 
            this.projektspeicherToolStripMenuItem.Name = "projektspeicherToolStripMenuItem";
            this.projektspeicherToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.projektspeicherToolStripMenuItem.Text = "Projekt speichern";
            // 
            // projektSchließenToolStripMenuItem
            // 
            this.projektSchließenToolStripMenuItem.Name = "projektSchließenToolStripMenuItem";
            this.projektSchließenToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.projektSchließenToolStripMenuItem.Text = "Projekt schließen";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(631, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Datenbank";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "externe Datei";
            // 
            // txtbox_VarTab_Pfad_ex
            // 
            this.txtbox_VarTab_Pfad_ex.Location = new System.Drawing.Point(85, 28);
            this.txtbox_VarTab_Pfad_ex.Name = "txtbox_VarTab_Pfad_ex";
            this.txtbox_VarTab_Pfad_ex.Size = new System.Drawing.Size(483, 20);
            this.txtbox_VarTab_Pfad_ex.TabIndex = 52;
            // 
            // textBox_VarTab_pfad
            // 
            this.textBox_VarTab_pfad.Location = new System.Drawing.Point(697, 28);
            this.textBox_VarTab_pfad.Name = "textBox_VarTab_pfad";
            this.textBox_VarTab_pfad.Size = new System.Drawing.Size(493, 20);
            this.textBox_VarTab_pfad.TabIndex = 53;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(574, 382);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 53);
            this.button1.TabIndex = 54;
            this.button1.Text = "→";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // VariablentabellenDialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1204, 827);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox_VarTab_pfad);
            this.Controls.Add(this.txtbox_VarTab_Pfad_ex);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox_Sheet_extern);
            this.Controls.Add(this.cboSheet);
            this.Controls.Add(this.dataGridView_TIAProKonf);
            this.Controls.Add(this.dataGridView_Extern);
            this.Name = "VariablentabellenDialogForm";
            this.Text = "Variablentabellen";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Extern)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_TIAProKonf)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView_Extern;
        private System.Windows.Forms.DataGridView dataGridView_TIAProKonf;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_Sheet_extern;
        private System.Windows.Forms.ComboBox cboSheet;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem tIAPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem öffnenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem excelToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem externeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aMLToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem speichernToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem speichernUnterToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem druckenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startTIAPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beendeTIAPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem verbindeTIAPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem sucheProjektToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projektspeicherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projektSchließenToolStripMenuItem;
        private System.Windows.Forms.TextBox txtbox_VarTab_Pfad_ex;
        private System.Windows.Forms.TextBox textBox_VarTab_pfad;
        private System.Windows.Forms.Button button1;
    }
}