﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TIAProKonf.Generals;
using TIAProKonf.Database;
using TIAProKonf.ImportFile;
using TIAProKonf.WorkOnTia;
using ExcelDataReader;
using System.IO;
using TIAProKonf.forms;
using System.Data.OleDb;

namespace TIAProKonf.forms
{
    public partial class BausteineDialogForm : Form
    {
        public BausteineDialogForm()
        {
            InitializeComponent();
            var pos = this.PointToScreen(label1.Location);
            pos = pictureBox1.PointToClient(pos);
            label1.Parent = pictureBox1;
            label1.Location = pos;
            label1.BackColor = Color.Transparent;




            treeView_Bausteinauswahl.Nodes.Add("A");
            treeView_Bausteinauswahl.Nodes.Add("B");
            treeView_Bausteinauswahl.Nodes.Add("Bibliotheksbausteine");

            treeView_Bausteinauswahl.Nodes[0].Nodes.Add("B001");
            treeView_Bausteinauswahl.Nodes[0].Nodes.Add("B002");
            treeView_Bausteinauswahl.Nodes[1].Nodes.Add("B003");

            treeView_Bausteinauswahl.Nodes[0].Nodes[0].Nodes.Add("Analog");
            treeView_Bausteinauswahl.Nodes[0].Nodes[0].Nodes.Add("Ventile");

            treeView_Bausteinauswahl.Nodes[0].Nodes[1].Nodes.Add("Analog");
            treeView_Bausteinauswahl.Nodes[0].Nodes[1].Nodes.Add("Ventile");

            treeView_Bausteinauswahl.Nodes[1].Nodes[0].Nodes.Add("Analog");
            treeView_Bausteinauswahl.Nodes[1].Nodes[0].Nodes.Add("Ventile");

            treeView_Bausteinauswahl.Nodes[0].Nodes[0].Nodes[1].Nodes.Add("V001");

            treeView_Bausteinauswahl.ExpandAll();


            comboBox1.SelectedIndex = 1;
            comboBox2.SelectedIndex = 1;
            comboBox3.SelectedIndex = 1;
            comboBox4.SelectedIndex = 1;
            comboBox5.SelectedIndex = 1;
            comboBox6.SelectedIndex = 1;
            comboBox7.SelectedIndex = 1;
            comboBox8.SelectedIndex = 1;
            comboBox9.SelectedIndex = 1;
            comboBox10.SelectedIndex = 1;
            comboBox11.SelectedIndex = 1;
            comboBox12.SelectedIndex = 1;
            comboBox13.SelectedIndex = 1;
            comboBox14.SelectedIndex = 1;
            comboBox15.SelectedIndex = 1;
            comboBox16.SelectedIndex = 1;
            comboBox17.SelectedIndex = 1;
            comboBox18.SelectedIndex = 1;
            comboBox19.SelectedIndex = 1;
            comboBox20.SelectedIndex = 1;
            comboBox21.SelectedIndex = 1;
            comboBox22.SelectedIndex = 1;
            comboBox23.SelectedIndex = 1;
            comboBox24.SelectedIndex = 1;




        }

        private void Label1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Zykluszeit des Bausteinaufrufs in Sec.",label1);
        }
    }
}
