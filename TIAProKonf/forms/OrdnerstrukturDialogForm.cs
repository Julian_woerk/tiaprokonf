﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TIAProKonf.Generals;
using TIAProKonf.Database;
using TIAProKonf.ImportFile;
using TIAProKonf.WorkOnTia;
using ExcelDataReader;
using System.IO;
using TIAProKonf.forms;

namespace TIAProKonf.forms
{
    public partial class OrdnerstrukturDialogForm : Form
    {

        private WorkOnDatabase _workOnDatabase4;
        public OrdnerstrukturDialogForm()
        {
            InitializeComponent();

            _workOnDatabase4 = new WorkOnDatabase();
            txtbox_Excel_Datenbank_Ordnerstruktur.Text = UserContainer.ExcelPathContainerDB;

            //UserContainer.ListboxWorkSheets = UserContainer.MyExcel.getWorksheets();
            //foreach (object item in UserContainer.ListboxWorkSheets)
            //    listbox_worksheets.Items.Add(item);

            //excelWorkBook = new MyExcelWorkBook(UserContainer.ExcelPathContainerDB);
            //UserContainer.MyExcelWorkSheet = new MyExcelWorkSheet(excelWorkBook.GetWorksheet("Ordnerstruktur"));

            treeView1.Nodes.Add("A");
            treeView1.Nodes.Add("B");
            treeView1.Nodes.Add("Bibliotheksbausteine");

            treeView1.Nodes[0].Nodes.Add("B001");
            treeView1.Nodes[0].Nodes.Add("B002");
            treeView1.Nodes[1].Nodes.Add("B003");

            treeView1.Nodes[0].Nodes[0].Nodes.Add("Analog");
            treeView1.Nodes[0].Nodes[0].Nodes.Add("Ventile");

            treeView1.Nodes[0].Nodes[1].Nodes.Add("Analog");
            treeView1.Nodes[0].Nodes[1].Nodes.Add("Ventile");

            treeView1.Nodes[1].Nodes[0].Nodes.Add("Ventil B");
            treeView1.Nodes[1].Nodes[0].Nodes.Add("Ventil C");

        }



        private void Btn_ExcelImport_Click(object sender, EventArgs e)
        {
            //excelWorkBook = new MyExcelWorkBook(UserContainer.ExcelPathContainerDB);
            //UserContainer.MyExcelWorkSheet = new MyExcelWorkSheet(excelWorkBook.GetWorksheet("Ordnerstruktur"));

            _workOnDatabase4.CreateFolderstructure();

        }

        private MyExcelWorkBook excelWorkBook = null; //============
                                                      //private String excelPath = null;//============


        private void Button1_Click(object sender, EventArgs e)
        {
            treeView1.Nodes.Add("Anlage A");
            treeView1.Nodes.Add("Anlage B");
            treeView1.Nodes.Add("Anlage C");

            treeView1.Nodes[0].Nodes.Add("Behälter 1");
            treeView1.Nodes[0].Nodes.Add("Behälter 2");
            treeView1.Nodes[0].Nodes.Add("Behälter 3");

            treeView1.Nodes[1].Nodes.Add("Behälter 4");
            treeView1.Nodes[1].Nodes.Add("Behälter 5");
            treeView1.Nodes[1].Nodes.Add("Behälter 6");

            treeView1.Nodes[0].Nodes[0].Nodes.Add("Ventil A");
            treeView1.Nodes[0].Nodes[0].Nodes.Add("Ventil B");
            treeView1.Nodes[0].Nodes[0].Nodes.Add("Ventil C");
        }

        private void Button_NodeDelete_Click(object sender, EventArgs e)
        {
            treeView1.SelectedNode.Remove();
        }

        private void Button_DeleteAll_Click(object sender, EventArgs e)
        {
            treeView1.Nodes.Clear();
        }

        private void Button_AddNodeParent_Click(object sender, EventArgs e)
        {
            treeView1.Nodes.Add(textBox_Parent.Text.ToString());
        }

        private void Button_AddNodeChild_Click(object sender, EventArgs e)
        {
            treeView1.Nodes[0].Nodes.Add(textBox_Child.Text.ToString());
        }

        private void Button_AddNodeGrandchild_Click(object sender, EventArgs e)
        {
            treeView1.Nodes[0].Nodes[0].Nodes.Add(textBox_Grandchild.Text.ToString());
        }

   




        //private void OrdnerstrukturDialogForm_Load(object sender, EventArgs e)
        //{
        //    foreach (TreeNode RN  in treeView1.Nodes)
        //    {
        //        RN.ContextMenuStrip = contextMenuStrip2;
        //        foreach (TreeNode CH in RN.Nodes)
        //        {
        //            CH.ContextMenuStrip = contextMenuStrip2;
        //        }
        //    }
        //}
    }
}





