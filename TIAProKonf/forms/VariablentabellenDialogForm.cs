﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TIAProKonf.Generals;
using TIAProKonf.Database;
using TIAProKonf.ImportFile;
using TIAProKonf.WorkOnTia;
using ExcelDataReader;
using System.IO;
using TIAProKonf.forms;
using System.Data.OleDb;

namespace TIAProKonf.forms
{
    public partial class VariablentabellenDialogForm : Form
    {

        private WorkOnDatabase _workOnDatabaseVarTab;
        public VariablentabellenDialogForm()
        {
            InitializeComponent();
            _workOnDatabaseVarTab = new WorkOnDatabase();
        }


        DataTableCollection tableCollectionExtern;
        private void ExterneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            switch (openFileDialog1.ShowDialog())
            {

                case DialogResult.OK:
                    //UserContainer.MyExcelWorkBookDB = new MyExcelWorkBook(openFileDialog1.FileName);  // Mit Alex'
                    //UserContainer.MyExcel = new Excel(openFileDialog1.FileName);

                    //_______ExcelDataReader_________

                    using (var stream = File.Open(openFileDialog1.FileName, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                            {
                                ConfigureDataTable = (_) => new ExcelDataTableConfiguration() { UseHeaderRow = true }
                            });
                            tableCollectionExtern = result.Tables;
                            comboBox_Sheet_extern.Items.Clear();
                            foreach (DataTable table in tableCollectionExtern)
                            {
                                comboBox_Sheet_extern.Items.Add(table.TableName);  // Worksheet zu ComboBox

                            }
                        }
                    }


                    UserContainer.MyExcelExtern = new Excel(openFileDialog1.FileName);
                    UserContainer.ExcelPathContainerExtern = openFileDialog1.FileName.ToString();
                    txtbox_VarTab_Pfad_ex.Text = UserContainer.ExcelPathContainerExtern;
                    //_______ExcelDataReaderEnde__________
                    break;

                default:
                    break;
                    
            }
          
        }

        private void ComboBox_Sheet_extern_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = tableCollectionExtern[comboBox_Sheet_extern.SelectedItem.ToString()];
            dataGridView_Extern.DataSource = dt;
        }


        private void Btn_ExcelImport_Click(object sender, EventArgs e)
        {
            _workOnDatabaseVarTab.CreateFolderstructure();
        }


        /*private MyExcelWorkBook excelWorkBook = null;*/ //============
        private String excelPath = null;//============

        DataTableCollection tableCollection;
        private void OpenExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            switch (openFileDialog1.ShowDialog())
            {

                case DialogResult.OK:

                    //_______ExcelDataReader_________
                    //=============
                    excelPath = openFileDialog1.FileName.ToString(); //============
                                                                     //=============

                    using (var stream = File.Open(openFileDialog1.FileName, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                            {
                                ConfigureDataTable = (_) => new ExcelDataTableConfiguration() { UseHeaderRow = true }
                            });
                            tableCollection = result.Tables;
                            cboSheet.Items.Clear();
                            foreach (DataTable table in tableCollection)
                            {
                                cboSheet.Items.Add(table.TableName);  // Worksheet zu ComboBox

                            }
                        }
                    }


                    UserContainer.MyExcelVar = new Excel(openFileDialog1.FileName);
                    UserContainer.ExcelPathContainerVar = openFileDialog1.FileName.ToString();
                    textBox_VarTab_pfad.Text = UserContainer.ExcelPathContainerVar;


                    ////_______ExcelDataReaderEnde__________

                    //UserContainer.MyExcel = new Excel(openFileDialog1.FileName);

                    //foreach (object item in UserContainer.MyExcel.getWorksheets())
                    //{
                    //    MessageBox.Show(item.ToString());

                    //}

                    //=============

                    //excelWorkBook = new MyExcelWorkBook(excelPath);
                    //UserContainer.MyExcelWorkSheet = new MyExcelWorkSheet(excelWorkBook.GetWorksheet("Ordnerstruktur"));
                    //============
                    //=============

                    break;

                default:
                    break;
            }

            //UserContainer.ListboxWorkSheets = UserContainer.MyExcel.getWorksheets();

            //foreach (object item in UserContainer.ListboxWorkSheets)
            //    listbox_worksheets.Items.Add(item);

            //txtbox_status.Text = UserContainer.StatusBarGUI;
            //===
            //===
            //===
            //openFileDialog1.ShowDialog();
            //UserContainer.MyExcelExtern = new Excel(openFileDialog1.FileName);
            //UserContainer.ExcelPathContainerExtern = openFileDialog1.FileName.ToString();            
            //textBox_VarTab_pfad.Text = UserContainer.ExcelPathContainerDB;

        }

        private void CboSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = tableCollection[cboSheet.SelectedItem.ToString()];
            dataGridView_TIAProKonf.DataSource = dt;
        }



        //private void Button1_Click(object sender, EventArgs e)
        //{
        //    //excelWorkBook = new MyExcelWorkBook(UserContainer.ExcelPathContainerDB);
        //    //UserContainer.MyExcelWorkSheet = new MyExcelWorkSheet(excelWorkBook.GetWorksheet("Variablentabelle"));


        //    //OleDbConnectionStringBuilder csbuilder = new OleDbConnectionStringBuilder();
        //    //csbuilder.Provider = "Microsoft.ACE.OLEDB.12.0";
        //    //csbuilder.DataSource = UserContainer.ExcelPathContainerDB;
        //    //csbuilder.Add("Extended Properties", "Excel 12.0 Xml;HDR=Yes");

        //    //using (OleDbConnection connection = new OleDbConnection(csbuilder.ConnectionString))
        //    //{
        //    //    DataTable sheet1 = new DataTable();
        //    //    connection.Open();
        //    //    string sqlQuery = @"SELECT * FROM [" + "Variablentabelle" + "]";
        //    //    using (OleDbDataAdapter adapter = new OleDbDataAdapter(sqlQuery, connection))
        //    //    {
        //    //        adapter.Fill(sheet1);
        //    //        dataGridView_test.DataSource = sheet1;
        //    //    }
        //    //    connection.Close();
        //    //}

        //    //System.Data.DataTable dt = null;
        //    //using (OleDbConnection connection = new OleDbConnection(csbuilder.ConnectionString))
        //    //{
        //    //    connection.Open();
        //    //    dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        //    //    connection.Close();
        //    //}

        //    //String[] excelSheets = new String[dt.Rows.Count];

        //    //int i = 0;
        //    //foreach (DataRow row in dt.Rows)
        //    //{
        //    //    excelSheets[i] = row["TABLE_NAME"].ToString();
        //    //    i++;
        //    //}


        //    //string constr = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source=" + UserContainer.ExcelPathContainerDB.ToString() + "; Extended Properties = \"Exel 8.0; HDR=Yes;\";";
        //    //OleDbConnection con = new OleDbConnection(constr);
        //    //OleDbDataAdapter sda = new OleDbDataAdapter("Select * From[" + "Variablentabelle" + "$']", con);
        //    //DataTable dt = new DataTable();
        //    //sda.Fill(dt);
        //    //dataGridView_test.DataSource = dt;



        //}
    }
}
