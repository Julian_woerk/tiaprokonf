﻿namespace TIAProKonf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tIAPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startTiaPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beendeTiaPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.verbindeTiaPortalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.sucheProjektToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speichernToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.schließeProjektToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.öffnenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.excelDatenbankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.externeExcelDateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.schließenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button_Ordnerstruktur = new System.Windows.Forms.Button();
            this.button_Hardware = new System.Windows.Forms.Button();
            this.button_Variablentabelle = new System.Windows.Forms.Button();
            this.button_Bausteine = new System.Windows.Forms.Button();
            this.listBox_Statusanzeige = new System.Windows.Forms.ListBox();
            this.listbox_worksheets = new System.Windows.Forms.CheckedListBox();
            this.btn_ExcelImport = new System.Windows.Forms.Button();
            this.comboBox_select_CPU = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_TIA_Projekt = new System.Windows.Forms.TextBox();
            this.txtbox_Excel_Datenbank = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_Bausteine_V2 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tIAPortalToolStripMenuItem,
            this.excelToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(672, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tIAPortalToolStripMenuItem
            // 
            this.tIAPortalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startTiaPortalToolStripMenuItem,
            this.beendeTiaPortalToolStripMenuItem,
            this.toolStripSeparator1,
            this.verbindeTiaPortalToolStripMenuItem,
            this.toolStripSeparator2,
            this.sucheProjektToolStripMenuItem,
            this.speichernToolStripMenuItem,
            this.schließeProjektToolStripMenuItem});
            this.tIAPortalToolStripMenuItem.Name = "tIAPortalToolStripMenuItem";
            this.tIAPortalToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.tIAPortalToolStripMenuItem.Text = "TIA Portal";
            // 
            // startTiaPortalToolStripMenuItem
            // 
            this.startTiaPortalToolStripMenuItem.Name = "startTiaPortalToolStripMenuItem";
            this.startTiaPortalToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.startTiaPortalToolStripMenuItem.Text = "Start TIA Portal";
            this.startTiaPortalToolStripMenuItem.Click += new System.EventHandler(this.StartTiaPortalToolStripMenuItem_Click);
            // 
            // beendeTiaPortalToolStripMenuItem
            // 
            this.beendeTiaPortalToolStripMenuItem.Enabled = false;
            this.beendeTiaPortalToolStripMenuItem.Name = "beendeTiaPortalToolStripMenuItem";
            this.beendeTiaPortalToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.beendeTiaPortalToolStripMenuItem.Text = "Beende TIA Portal";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(171, 6);
            // 
            // verbindeTiaPortalToolStripMenuItem
            // 
            this.verbindeTiaPortalToolStripMenuItem.Name = "verbindeTiaPortalToolStripMenuItem";
            this.verbindeTiaPortalToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.verbindeTiaPortalToolStripMenuItem.Text = "Verbinde TIA Portal";
            this.verbindeTiaPortalToolStripMenuItem.Click += new System.EventHandler(this.VerbindeTiaPortalToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(171, 6);
            // 
            // sucheProjektToolStripMenuItem
            // 
            this.sucheProjektToolStripMenuItem.Name = "sucheProjektToolStripMenuItem";
            this.sucheProjektToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.sucheProjektToolStripMenuItem.Text = "Projekt öffnen";
            this.sucheProjektToolStripMenuItem.Click += new System.EventHandler(this.SucheProjektToolStripMenuItem_Click);
            // 
            // speichernToolStripMenuItem
            // 
            this.speichernToolStripMenuItem.Name = "speichernToolStripMenuItem";
            this.speichernToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.speichernToolStripMenuItem.Text = "Projekt speichern";
            this.speichernToolStripMenuItem.Click += new System.EventHandler(this.SpeichernToolStripMenuItem_Click);
            // 
            // schließeProjektToolStripMenuItem
            // 
            this.schließeProjektToolStripMenuItem.Enabled = false;
            this.schließeProjektToolStripMenuItem.Name = "schließeProjektToolStripMenuItem";
            this.schließeProjektToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.schließeProjektToolStripMenuItem.Text = "Projekt schließen";
            this.schließeProjektToolStripMenuItem.Click += new System.EventHandler(this.SchließeProjektToolStripMenuItem_Click);
            // 
            // excelToolStripMenuItem
            // 
            this.excelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.öffnenToolStripMenuItem1,
            this.schließenToolStripMenuItem1});
            this.excelToolStripMenuItem.Name = "excelToolStripMenuItem";
            this.excelToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.excelToolStripMenuItem.Text = "Excel";
            // 
            // öffnenToolStripMenuItem1
            // 
            this.öffnenToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.excelDatenbankToolStripMenuItem,
            this.externeExcelDateiToolStripMenuItem});
            this.öffnenToolStripMenuItem1.Name = "öffnenToolStripMenuItem1";
            this.öffnenToolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.öffnenToolStripMenuItem1.Text = "Öffnen";
            // 
            // excelDatenbankToolStripMenuItem
            // 
            this.excelDatenbankToolStripMenuItem.Name = "excelDatenbankToolStripMenuItem";
            this.excelDatenbankToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.excelDatenbankToolStripMenuItem.Text = "Excel-Datenbank";
            this.excelDatenbankToolStripMenuItem.Click += new System.EventHandler(this.ExcelDatenbankToolStripMenuItem_Click);
            // 
            // externeExcelDateiToolStripMenuItem
            // 
            this.externeExcelDateiToolStripMenuItem.Name = "externeExcelDateiToolStripMenuItem";
            this.externeExcelDateiToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.externeExcelDateiToolStripMenuItem.Text = "externe Excel-Datei";
            this.externeExcelDateiToolStripMenuItem.Click += new System.EventHandler(this.ExterneExcelDateiToolStripMenuItem_Click);
            // 
            // schließenToolStripMenuItem1
            // 
            this.schließenToolStripMenuItem1.Name = "schließenToolStripMenuItem1";
            this.schließenToolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.schließenToolStripMenuItem1.Text = "Schließen";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 201);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Statusanzeige";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button_Ordnerstruktur
            // 
            this.button_Ordnerstruktur.Enabled = false;
            this.button_Ordnerstruktur.Location = new System.Drawing.Point(131, 154);
            this.button_Ordnerstruktur.Name = "button_Ordnerstruktur";
            this.button_Ordnerstruktur.Size = new System.Drawing.Size(117, 38);
            this.button_Ordnerstruktur.TabIndex = 10;
            this.button_Ordnerstruktur.Text = "Ordnerstruktur";
            this.button_Ordnerstruktur.UseVisualStyleBackColor = true;
            this.button_Ordnerstruktur.Click += new System.EventHandler(this.Button_Ordnerstruktur_Click);
            // 
            // button_Hardware
            // 
            this.button_Hardware.Enabled = false;
            this.button_Hardware.Location = new System.Drawing.Point(8, 154);
            this.button_Hardware.Name = "button_Hardware";
            this.button_Hardware.Size = new System.Drawing.Size(117, 38);
            this.button_Hardware.TabIndex = 11;
            this.button_Hardware.Text = "Hardware";
            this.button_Hardware.UseVisualStyleBackColor = true;
            this.button_Hardware.Click += new System.EventHandler(this.Button_Hardware_Click);
            // 
            // button_Variablentabelle
            // 
            this.button_Variablentabelle.Enabled = false;
            this.button_Variablentabelle.Location = new System.Drawing.Point(254, 154);
            this.button_Variablentabelle.Name = "button_Variablentabelle";
            this.button_Variablentabelle.Size = new System.Drawing.Size(117, 38);
            this.button_Variablentabelle.TabIndex = 19;
            this.button_Variablentabelle.Text = "Variablentabellen";
            this.button_Variablentabelle.UseVisualStyleBackColor = true;
            this.button_Variablentabelle.Click += new System.EventHandler(this.Button_Variablentabelle_Click);
            // 
            // button_Bausteine
            // 
            this.button_Bausteine.Enabled = false;
            this.button_Bausteine.Location = new System.Drawing.Point(377, 154);
            this.button_Bausteine.Name = "button_Bausteine";
            this.button_Bausteine.Size = new System.Drawing.Size(117, 38);
            this.button_Bausteine.TabIndex = 20;
            this.button_Bausteine.Text = "Bausteine";
            this.button_Bausteine.UseVisualStyleBackColor = true;
            this.button_Bausteine.Click += new System.EventHandler(this.Button_Bausteine_Click);
            // 
            // listBox_Statusanzeige
            // 
            this.listBox_Statusanzeige.FormattingEnabled = true;
            this.listBox_Statusanzeige.Location = new System.Drawing.Point(10, 217);
            this.listBox_Statusanzeige.Name = "listBox_Statusanzeige";
            this.listBox_Statusanzeige.Size = new System.Drawing.Size(348, 134);
            this.listBox_Statusanzeige.TabIndex = 22;
            // 
            // listbox_worksheets
            // 
            this.listbox_worksheets.FormattingEnabled = true;
            this.listbox_worksheets.Location = new System.Drawing.Point(510, 38);
            this.listbox_worksheets.Name = "listbox_worksheets";
            this.listbox_worksheets.Size = new System.Drawing.Size(150, 229);
            this.listbox_worksheets.TabIndex = 26;
            // 
            // btn_ExcelImport
            // 
            this.btn_ExcelImport.Location = new System.Drawing.Point(378, 271);
            this.btn_ExcelImport.Name = "btn_ExcelImport";
            this.btn_ExcelImport.Size = new System.Drawing.Size(116, 23);
            this.btn_ExcelImport.TabIndex = 29;
            this.btn_ExcelImport.Text = "Import nach TIA";
            this.btn_ExcelImport.UseVisualStyleBackColor = true;
            this.btn_ExcelImport.Click += new System.EventHandler(this.Btn_ExcelImport_Click_1);
            // 
            // comboBox_select_CPU
            // 
            this.comboBox_select_CPU.FormattingEnabled = true;
            this.comboBox_select_CPU.Location = new System.Drawing.Point(510, 286);
            this.comboBox_select_CPU.Name = "comboBox_select_CPU";
            this.comboBox_select_CPU.Size = new System.Drawing.Size(150, 21);
            this.comboBox_select_CPU.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(507, 270);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "Auswahl CPU:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(378, 244);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 23);
            this.button1.TabIndex = 32;
            this.button1.Text = "Export von TIA";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Excel-Datenbank";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "TIA Portal Projekt";
            // 
            // textBox_TIA_Projekt
            // 
            this.textBox_TIA_Projekt.Location = new System.Drawing.Point(99, 23);
            this.textBox_TIA_Projekt.Name = "textBox_TIA_Projekt";
            this.textBox_TIA_Projekt.Size = new System.Drawing.Size(385, 20);
            this.textBox_TIA_Projekt.TabIndex = 24;
            // 
            // txtbox_Excel_Datenbank
            // 
            this.txtbox_Excel_Datenbank.Location = new System.Drawing.Point(99, 49);
            this.txtbox_Excel_Datenbank.Name = "txtbox_Excel_Datenbank";
            this.txtbox_Excel_Datenbank.Size = new System.Drawing.Size(385, 20);
            this.txtbox_Excel_Datenbank.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtbox_Excel_Datenbank);
            this.groupBox1.Controls.Add(this.textBox_TIA_Projekt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(10, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(494, 80);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Speicherpfade";
            // 
            // button_Bausteine_V2
            // 
            this.button_Bausteine_V2.Location = new System.Drawing.Point(377, 201);
            this.button_Bausteine_V2.Name = "button_Bausteine_V2";
            this.button_Bausteine_V2.Size = new System.Drawing.Size(117, 23);
            this.button_Bausteine_V2.TabIndex = 34;
            this.button_Bausteine_V2.Text = "Bausteine V2";
            this.button_Bausteine_V2.UseVisualStyleBackColor = true;
            this.button_Bausteine_V2.Click += new System.EventHandler(this.Button_Bausteine_V2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 365);
            this.Controls.Add(this.button_Bausteine_V2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox_select_CPU);
            this.Controls.Add(this.btn_ExcelImport);
            this.Controls.Add(this.listbox_worksheets);
            this.Controls.Add(this.listBox_Statusanzeige);
            this.Controls.Add(this.button_Bausteine);
            this.Controls.Add(this.button_Variablentabelle);
            this.Controls.Add(this.button_Ordnerstruktur);
            this.Controls.Add(this.button_Hardware);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "TIAProKon";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tIAPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startTiaPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beendeTiaPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verbindeTiaPortalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem schließeProjektToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sucheProjektToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem speichernToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button_Ordnerstruktur;
        private System.Windows.Forms.Button button_Hardware;
        private System.Windows.Forms.Button button_Variablentabelle;
        private System.Windows.Forms.Button button_Bausteine;
        private System.Windows.Forms.ListBox listBox_Statusanzeige;
        private System.Windows.Forms.ToolStripMenuItem excelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem öffnenToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem excelDatenbankToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem externeExcelDateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem schließenToolStripMenuItem1;
        public System.Windows.Forms.CheckedListBox listbox_worksheets;
        private System.Windows.Forms.Button btn_ExcelImport;
        private System.Windows.Forms.ComboBox comboBox_select_CPU;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_TIA_Projekt;
        private System.Windows.Forms.TextBox txtbox_Excel_Datenbank;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button_Bausteine_V2;
    }
}

