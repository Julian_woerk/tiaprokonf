﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TIAProKonf.Generals;
using TIAProKonf.Database;
using TIAProKonf.ImportFile;
using TIAProKonf.WorkOnTia;
using ExcelDataReader;
using System.IO;
using TIAProKonf.forms;



namespace TIAProKonf
{
    public partial class Form1 : Form
    {
        private TiaOpennessAccess _tiaOpennessAccess;
        private WorkOnDatabase _workOnDatabase;
        private MyExcelWorkBook excelWorkBook = null;


        public Form1()
        {
            InitializeComponent();
            AppDomain CurrentDomain = AppDomain.CurrentDomain;
            CurrentDomain.AssemblyResolve += new ResolveEventHandler(TiaOpennessAccess.MyResolver);
            _tiaOpennessAccess = new TiaOpennessAccess();
            _workOnDatabase = new WorkOnDatabase();
            listBox_Statusanzeige.Items.Add("Arbeitsschritte:");
            listBox_Statusanzeige.Items.Add("- TIA Portal öffnen");
            listBox_Statusanzeige.Items.Add("- Projekt öffnen");
            listBox_Statusanzeige.Items.Add("- Excel-Datenbank öffnen");
            listBox_Statusanzeige.Items.Add("- Planungsdaten anpassen");


        }

        //Änderung zu hause


        private void StartTiaPortalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _tiaOpennessAccess.StartTiaPortal();
            
            beendeTiaPortalToolStripMenuItem.Enabled = true;
            

        }

        private void VerbindeTiaPortalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _tiaOpennessAccess.ConnectToTiaPortal();
            textBox_TIA_Projekt.Text = UserContainer.Project.Path.ToString();
            schließeProjektToolStripMenuItem.Enabled = true;
            comboBox_select_CPU.Items.Add("PLC_1 [1516-3 PN/DP]");
        }

        private void SchließeProjektToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                _tiaOpennessAccess.CloseProject();
                MessageBox.Show("Projekt wurde geschlossen");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SucheProjektToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _tiaOpennessAccess.SearchProject();
        }

        private void SpeichernToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _tiaOpennessAccess.SaveProject();

        }    

        //private void Button2_Click(object sender, EventArgs e)
        //{
        //    openFileDialog1.ShowDialog();
        //    UserContainer.MyExcel = new Excel(openFileDialog1.FileName);
        //    MessageBox.Show(UserContainer.MyExcel.readCell(0, 0));

        //    UserContainer.MyExcel.closeFile();
        //}

        private void Btn_ExcelImport_Click(object sender, EventArgs e)
        {
            //CreateFolderstructure();
            _workOnDatabase.CreateFolderstructure();
        }

        private void Button_Ordnerstruktur_Click(object sender, EventArgs e)
        {
            new OrdnerstrukturDialogForm().ShowDialog();
        }
        private void Button_Hardware_Click(object sender, EventArgs e)
        {
            new HardwareDialogForm().ShowDialog();
        }

        private void Button_Variablentabelle_Click(object sender, EventArgs e)
        {
            new VariablentabellenDialogForm().ShowDialog();
        }

        private void Button_Bausteine_Click(object sender, EventArgs e)
        {
            new BausteineDialogForm().ShowDialog();
        }
        private void Button_Bausteine_V2_Click(object sender, EventArgs e)
        {
            new BausteinDialogFormv2().ShowDialog();
        }



        private void Button6_Click(object sender, EventArgs e)
        {
            //UserContainer.MyExcel.closeFile();
        }
        
        private void ExcelDatenbankToolStripMenuItem_Click(object sender, EventArgs e)
        {

            openFileDialog1.ShowDialog();
            
            UserContainer.MyExcel = new Excel(openFileDialog1.FileName);
            UserContainer.ExcelPathContainerDB = openFileDialog1.FileName.ToString();
            MessageBox.Show("Gratuliere, Sie habe die Excel-Datenbank erfolgreich geöffnet.");
            txtbox_Excel_Datenbank.Text = UserContainer.ExcelPathContainerDB;
            button_Ordnerstruktur.Enabled = true;
            button_Hardware.Enabled = true;
            button_Variablentabelle.Enabled = true;
            button_Bausteine.Enabled = true;
            //UserContainer.MyExcel.closeFile();
            excelWorkBook = new MyExcelWorkBook(UserContainer.ExcelPathContainerDB);
            UserContainer.MyExcelWorkSheet = new MyExcelWorkSheet(excelWorkBook.GetWorksheet("Ordnerstruktur"));
            UserContainer.ListboxWorkSheets = UserContainer.MyExcel.getWorksheets();
            foreach (object item in UserContainer.MyExcel.getWorksheets())
                listbox_worksheets.Items.Add(item);





        }

        private void ExterneExcelDateiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            UserContainer.MyExcelExtern = new Excel(openFileDialog1.FileName);
            UserContainer.ExcelPathContainerExtern = openFileDialog1.FileName.ToString();
            MessageBox.Show(UserContainer.MyExcelExtern.readCell(0, 0));

            //UserContainer.MyExcel.closeFile();
        }

        private void Btn_ExcelImport_Click_1(object sender, EventArgs e)
        {
            _workOnDatabase.CreateFolderstructure();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            foreach (object item in UserContainer.MyExcel.getWorksheets())
                listbox_worksheets.Items.Add(item);
        }

    
    }
}
