﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIAProKonf.ImportFile;
using Siemens.Engineering;

namespace TIAProKonf.Database
{
    public static class UserContainer
    {
        private static string _orderNoGUI;
        public static string OrderNoGUI
        {
            get { return _orderNoGUI; }
            set { _orderNoGUI = value; }
        }


        private static string _deviceNameGUI;
        public static string DeviceNameGUI
        {
            get { return _deviceNameGUI; }
            set { _deviceNameGUI = value; }
        }


        private static string _versionNoGUI;
        public static string VersionNoGUI
        {
            get { return _versionNoGUI; }
            set { _versionNoGUI = value; }
        }

        private static string _statusBarGUI;
        public static string StatusBarGUI
        {
            get { return _statusBarGUI; }
            set { _statusBarGUI = value; }
        }

        private static List<string> _listboxWorkSheets;
        public static List<string> ListboxWorkSheets
        {
            get { return _listboxWorkSheets; }
            set { _listboxWorkSheets = value; }
        }

        //private static Excel _excelTarget;
        //public static Excel ExcelTarget
        //{
        //    get { return _excelTarget; }
        //    set { _excelTarget = value; }
        //}

        private static MyExcelWorkBook _myExcelWorkBookDB;        
        public static MyExcelWorkBook MyExcelWorkBookDB
        {
            get { return _myExcelWorkBookDB; }
            set { _myExcelWorkBookDB = value; }
        }

        private static MyExcelWorkSheet _myExcelWorkSheet;
        public static MyExcelWorkSheet MyExcelWorkSheet
        {
            get { return _myExcelWorkSheet; }
            set { _myExcelWorkSheet = value; }
        }

        //==========================
        private static Excel _myExcel;
        public static Excel MyExcel
        {
            get { return _myExcel; }
            set { _myExcel = value; }
        }
        private static string _excelPathContainerDB;
        public static string ExcelPathContainerDB
        {
            get { return _excelPathContainerDB; }
            set { _excelPathContainerDB = value; }
        }
        //=========================


             //Variablentabelle
        //=========================
        private static Excel _myExcelExtern;
        public static Excel MyExcelExtern
        {
            get { return _myExcelExtern; }
            set { _myExcelExtern = value; }
        }

        private static string _excelPathContainerExtern;
        public static string ExcelPathContainerExtern
        {
            get { return _excelPathContainerExtern; }
            set { _excelPathContainerExtern = value; }
        }

        private static Excel _myExcelVar;
        public static Excel MyExcelVar
        {
            get { return _myExcelVar; }
            set { _myExcelVar = value; }
        }

        private static string _excelPathContainerVar;
        public static string ExcelPathContainerVar
        {
            get { return _excelPathContainerVar; }
            set { _excelPathContainerVar = value; }
        }
        //=========================

        private static Project _project;
        public static Project Project
        {
            get { return _project; }
            set { _project = value; }
        }

        private static System.IO.FileStream _streamDatagridviewExcelDB;
        public static System.IO.FileStream StreamDatagridviewExcelDB
        {
            get { return _streamDatagridviewExcelDB; }
            set { _streamDatagridviewExcelDB = value; }
        }



    }
}
