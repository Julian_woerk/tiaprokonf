﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using _Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Globalization;


namespace TIAProKonf.Database
{

    public class MyExcelWorkBook
    {
        //Diese Klasse öffnet mit Hilfe der bereitgestellten Excelklasse eine Excel-Datei
        private _Excel.Application xlApp;
        private _Excel.Workbook xlWorkBook;
        //private _Excel.Worksheet xlWorkSheet;
        private CultureInfo culture = new CultureInfo("de-DE");

        public MyExcelWorkBook(string excelPath)
        {
            xlApp = new _Excel.Application();
            xlApp.Visible = true;

            object missing = Missing.Value;
            //Excel-Datei öffnen
            xlWorkBook = xlApp.Workbooks.Open(excelPath, missing, false, missing,
                missing, missing, missing, missing, missing, missing, missing, missing, missing,
                missing, missing);
        }

        //Diese Methode gibt eine Liste mit allen in der Excel-Datei vorhandenen Tabellen (Worksheets) zurück
        //public List<object> ListOfWorksheetName()
        //{
        //    List<object> xlWorkSheetsList = new List<object>();
        //    for (int i = 1; i <= xlWorkBook.Sheets.Count; i++)
        //    {
        //        xlWorkSheet = (_Excel.Worksheet)xlWorkBook.Worksheets[i];
        //        xlWorkSheetsList.Add((object)xlWorkSheet.Name.ToString());
        //    }
        //    return xlWorkSheetsList;
        //}

        public List<string> getWorksheets1()
        {
            List<string> returnList = new List<string>();
            foreach (Worksheet WS in xlWorkBook.Worksheets)
            {
                returnList.Add(WS.Name);
            }
            return returnList;
        }

        //Mit dieser Methode werden wird ein Worksheet Objekt der ExcelKlasse zurückgegeben
        public _Excel.Worksheet GetWorksheet(string name)
        {
            foreach (_Excel.Worksheet item in xlWorkBook.Worksheets)
            {
                if (name.ToUpper().Equals(item.Name.ToString().ToUpper()))
                {
                    return item;
                }
            }
            return null;
        }

        public void SaveAndClose()
        {
            DateTime dateTime = DateTime.Now;
            string newFilename = xlWorkBook.Path + "\\" + xlWorkBook.Name;

            newFilename = newFilename.Insert(newFilename.IndexOf("."), "_" + dateTime.Year.ToString() + dateTime.Month.ToString() +
                dateTime.Day.ToString() + "_" + dateTime.Hour.ToString() + dateTime.Minute.ToString());
            xlWorkBook.Close(true, newFilename, Missing.Value);
            xlApp.Application.Quit();
            xlApp.Quit();
        }
    }

    public class MyExcelWorkSheet
    {
        //Über diese Klasse kann besser auf die Daten aus einer Excel-Tabelle zugegriffen werden
        private _Excel.Worksheet xlWorkSheet;
        private Dictionary<string, string> infos = new Dictionary<string, string>();
        private _Excel.Range headline = null;
        private _Excel.Range data = null;

        public MyExcelWorkSheet(MyExcelWorkBook xlWorkBook, string nameOfWorksheet)
        {
            xlWorkSheet = xlWorkBook.GetWorksheet(nameOfWorksheet);
            setExcelRange();
        }

        public MyExcelWorkSheet(_Excel.Worksheet xlWorkSheet)
        {
            this.xlWorkSheet = xlWorkSheet;
            setExcelRange();
        }

        private void setExcelRange()
        {
            headline = xlWorkSheet.Range[xlWorkSheet.Cells[7, "A"], xlWorkSheet.Cells[7, xlWorkSheet.UsedRange.Columns.Count]];
            data = xlWorkSheet.Range[xlWorkSheet.Cells[11, "A"], xlWorkSheet.Cells[xlWorkSheet.UsedRange.Rows.Count, xlWorkSheet.UsedRange.Columns.Count]];

            for (int i = 1; i < 7; i++)
            {
                if (xlWorkSheet.Cells[i, 1].Value != null && xlWorkSheet.Cells[i, 2].Value != null)
                {
                    if (!infos.ContainsKey(xlWorkSheet.Cells[i, 1].Value.ToString()))
                    {
                        infos.Add(xlWorkSheet.Cells[i, 1].Value.ToString(), xlWorkSheet.Cells[i, 2].Value.ToString());
                    }
                }
            }
        }

        public Dictionary<string, string> Infos
        {
            get { return infos; }
        }

        /// <summary>
        /// Gibt den Integerwert einer Spalte zurück.
        /// </summary>
        /// <param name="name">Name der Spalte - Groß- und Kleinschreibung wird nicht beachtet</param>
        /// <returns>Integerwert der Spalte. Wird die Spalte nicht gefunden wird eine -1 zurückgegeben</returns>
        public int GetColumnNumber(string name)
        {
            foreach (Microsoft.Office.Interop.Excel.Range cell in headline)
            {
                if (cell.Value != null)
                {
                    if (cell.Value.ToString().ToUpper().Equals(name.ToUpper())) return cell.Column;
                }
            }
            return -1;
        }

        /// <summary>
        /// Gibt die Daten einer Zelle als Object zurück
        /// </summary>
        /// <param name="rowIndex">Zeilennummer</param>
        /// <param name="columnIndex">Spaltennummer</param>
        /// <returns>Gibt ein Object zurück. Ist der ColumnIndex oder RowIndex kleiner 1 wird ein NULL-Objekt zurück gegeben </returns>
        public object GetData(int rowIndex, int columnIndex)
        {
            if (rowIndex < 1) return null;
            if (columnIndex < 1) return null;
            return data.Cells[rowIndex, columnIndex].Value;
        }
        /// <summary>
        /// Gibt eine Zelle der Spaltenüberschriften zurück
        /// </summary>
        /// <param name="columnIndex">Spaltennummer</param>
        /// <returns>Gibt einen String zurück. Ist der ColumnIndex kleiner 1 wird ein NULL-Objekt zurück gegeben </returns>
        public string GetHeadline(int columnIndex)
        {
            if (columnIndex < 1) return null;
            if (headline.Cells[1, columnIndex].Value == null) return null;
            return headline.Cells[1, columnIndex].Value.ToString();
        }

        /// <summary>
        /// Gibt eine Zelle der im Datenbereich zurück
        /// </summary>
        /// <param name="rowIndex">Zeilennummer</param>
        /// <param name="columnName">Name der Spalte</param>
        /// <returns>Gibt ein Object zurück. Ist der RowIndex kleiner 1 wird ein NULL-Objekt zurück gegeben </returns>
        public object GetData(int rowIndex, string columnName)
        {
            //if (columnName.ToUpper().Equals("logicalAddress".ToUpper()))
            //    return GetLogicalAddress(rowIndex);
            //if (rowIndex < 1)
            //    return null;
            //if (GetColumnNumber(columnName) != -1)
            //    return data.Cells[rowIndex, GetColumnNumber(columnName)].Value;
            //return null;

            if (columnName.ToUpper().Equals("logicalAddress".ToUpper()))
                return GetLogicalAddress(rowIndex);
            if (rowIndex < 1)
                return null;
            if (GetColumnNumber(columnName) != -1)
                return data.Cells[rowIndex, GetColumnNumber(columnName)].Value;
            return null;
        }

        private object GetLogicalAddress(int rowIndex)
        {
            int column = GetColumnNumber("Adresse");
            string str = "%" + GetData(rowIndex, column).ToString() + GetData(rowIndex, column + 1).ToString();
            if (str.IndexOf(",") != -1)
            {
                str = str.Replace(",", ".");
            }
            else if (str.IndexOf(",") == -1) str = str + ".0";
            return str;
        }

        /// <summary>
        /// Gibt die Anzahl der verwendeten Reihen zurück
        /// </summary>
        public int DataRows
        {
            get { return data.Rows.Count; }
        }

        /// <summary>
        /// Gibt die Anzahl der verwendeten Spalten zurück
        /// </summary>
        public int DataColumns
        {
            get { return data.Columns.Count; }
        }


        /// <summary>
        /// zum Schreiben der Verwendeten Hardware
        /// </summary>
        public void SetData(List<object> dataList)
        {
            int column = 1;
            int row = 0;
            row = DataRows + 1;
            foreach (object item in dataList)
            {
                data.Cells[row, column].Value = item;
                column++;
            }
            setExcelRange();
        }
    }
}
