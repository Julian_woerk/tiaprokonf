﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using _excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Globalization;
using System.Windows.Forms;
using System.IO;
using Siemens.Engineering;
using Siemens.Engineering.HW;
using Siemens.Engineering.HW.Features;
using Siemens.Engineering.SW;
using Siemens.Engineering.SW.Blocks;

using TIAProKonf.ImportFile;
using TIAProKonf.WorkOnTia;

namespace TIAProKonf.Database
{
    class WorkOnDatabase
    {
        //private TiaPortalProcess objTiaPortalProcess;
        //private TiaPortal objTiaPortal;


        //public void ExcelWorkSheets()
        //{
        //    UserContainer.ListboxWorkSheets = UserContainer.ExcelTarget.getWorksheets();
        //}


        public void CreateFolderstructure()
        {
            foreach (object item in UserContainer.ListboxWorkSheets)
            {
                if (item.ToString().Equals("Ordnerstruktur"))
                {
                    FolderStructure.StructCreate(UserContainer.Project, UserContainer.MyExcelWorkSheet);


                }
            }
        }
    }
}
