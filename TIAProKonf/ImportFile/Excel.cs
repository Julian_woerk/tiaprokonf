﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using _Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Globalization;

namespace TIAProKonf.ImportFile
{
    public class Excel
    {

        string path = "";
        _Application excel = new _Excel.Application();
        public Workbook wb;
        public Worksheet ws;

        private List<string> writtenCols = new List<string>();

        public Excel(string path, int sheet = 0)
        {
            this.path = path;
            wb = excel.Workbooks.Open(path);
            selectSheet(sheet);
        }

        public void calcWrittenCols()
        {
            writtenCols.Clear();
            for (int i = 1; i < ws.UsedRange.Columns.Count; i++)
            {
                writtenCols.Add("[" + writtenCols.Count + "] " + ws.UsedRange.Cells[1, i].Value2);
                // TODO: column letter instead of index - would be nice - maybe
                //änderung
            }
        }

        public List<string> getWorksheets()
        {
            List<string> returnList = new List<string>();
            foreach (Worksheet WS in wb.Worksheets)
            {
                returnList.Add(WS.Name);
            }
            return returnList;
        }

        public List<string> getWrittenCols()
        {
            List<string> returnList = new List<string>();
            //List<T>.ForEach(Lambda-Expression) iterriert über alle elemente in der Liste und führt Lambda auf dieses Objekt aus
            writtenCols.ForEach(COL => returnList.Add((string)COL.Clone()));

            //foreach (string COLS in writtenCols)
            //{
            //    returnList.Add((string)COLS.Clone());
            //}

            return returnList;
        }

        public void closeFile()
        {
            wb.Close();
        }

        public string readCell(int row, int col)
        {
            row++;
            col++;
            if (ws.Cells[row, col].Value2 != null) return ws.Cells[row, col].Text;
            return "";
        }


        public string[,] readRange(int start_row, int start_col, int end_row, int end_col)
        {
            Range range = (Range)ws.Range[ws.Cells[start_row, start_col], ws.Cells[end_row, end_col]];
            object[,] holder = range.Value2;
            string[,] returnString = new string[end_row - start_row, end_col - start_col];
            for (int p = 1; p < end_row - start_row; p++)
            {
                for (int q = 1; q < end_col - end_col; q++)
                {
                    returnString[p - 1, q - 1] = holder[p, q].ToString();
                }
            }
            return returnString;
        }

        public void writeCell(int row, int col, string value)
        {
            row++;
            col++;
            ws.Cells[row, col].Value2 = value;
        }


        public void save()
        {
            wb.Save();
        }

        public void saveAs(string path)
        {
            wb.SaveAs(path);
        }

        public void createNewFile()
        {
            this.wb = excel.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            this.ws = wb.Worksheets[1];
        }

        public void createNewSheet()
        {
            Worksheet tempSheet = wb.Worksheets.Add(After: ws);
        }

        public void selectSheet(int sheetNumber)
        {
            this.ws = wb.Worksheets[sheetNumber + 1];
            this.ws.Activate();
            calcWrittenCols();
        }

        public void deleteSheet(int sheetNumber)
        {
            wb.Worksheets[sheetNumber + 1].Delete();
        }

        public void protectSheet()
        {
            ws.Protect();
        }

        public void protectSheet(string password)
        {
            ws.Protect(password);
        }

        public void unprotectSheet()
        {
            ws.Unprotect();
        }

        public void unprotectSheet(string password)
        {
            ws.Unprotect(password);
        }
    }
}
