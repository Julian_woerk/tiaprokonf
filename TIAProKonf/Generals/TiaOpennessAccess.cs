﻿using Microsoft.Win32;
using Siemens.Engineering;
using Siemens.Engineering.Hmi;
using Siemens.Engineering.HW;
using Siemens.Engineering.HW.Features;
using Siemens.Engineering.SW;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TIAProKonf.Database;


namespace TIAProKonf.Generals
{
    public class TiaOpennessAccess
    {

        public static TiaPortalProcess objTiaPortalProcess;
        public static TiaPortal objTiaPortal;
        

        public static Assembly MyResolver(object sender, ResolveEventArgs args)
        {
            int index = args.Name.IndexOf(',');
            if (index == -1)
            {
                return null;
            }
            string name = args.Name.Substring(0, index);

            RegistryKey filePathReg = Registry.LocalMachine.OpenSubKey(
                "SOFTWARE\\Siemens\\Automation\\Openness\\15.1\\PublicAPI\\15.1.0.0");

            if (filePathReg == null)
                return null;

            object oRegKeyValue = filePathReg.GetValue(name);
            if (oRegKeyValue != null)
            {
                string filePath = oRegKeyValue.ToString();

                string path = filePath;
                string fullPath = Path.GetFullPath(path);
                if (File.Exists(fullPath))
                {
                    return Assembly.LoadFrom(fullPath);
                }
            }

            return null;
        }

        public void StartTiaPortal()
        {
            objTiaPortal = new TiaPortal(TiaPortalMode.WithUserInterface);

        }


        public void ConnectToTiaPortal()
        {

            //IList<TiaPortalProcess> processes = TiaPortal.GetProcesses();
            //try
            //{
            //    objTiaPortalProcess = processes[0];
            //    objTiaPortal = objTiaPortalProcess.Attach();
            //    //if (objTiaPortal.GetCurrentProcess().Mode == TiaPortalMode.WithUserInterface)
            //    //{
            //    //    rdb_WithUI.Checked = true;
            //    //}
            //    //else
            //    //{
            //    //    rdb_WithoutUI.Checked = true;
            //    //}


            //    if (objTiaPortal.Projects.Count <= 0)
            //    {
            //        System.Windows.Forms.MessageBox.Show("No TIA Portal Project was found!");
            //    }

            //    objProject = objTiaPortal.Projects[0];


            //    //txt_Status.Text = "No running instance of TIA Portal was found!";
            //    //btn_Connect.Enabled = true;
            //    //return;
            //    //txt_Status.Text = "More than one running instance of TIA Portal was found!";
            //    //btn_Connect.Enabled = true;
            //    //return;
            //}
            //catch
            //{
            //    System.Windows.Forms.MessageBox.Show("Test");
            //}


            //if (objTiaPortal is null)
            //{
                IList<TiaPortalProcess> processes = TiaPortal.GetProcesses();
                switch (processes.Count)
                { //TODO: auflistung falls mehrere Projekte geöffnet sind
                    case 1:
                        objTiaPortalProcess = processes[0];
                        objTiaPortal = objTiaPortalProcess.Attach();
                        UserContainer.Project = objTiaPortal.Projects[0];

                        //foreach (Device item in objProject.Devices)
                        //{
                        //    Console.WriteLine(item.Name);
                        //}
                        System.Windows.Forms.MessageBox.Show("Connect to TIA");
                        break;
                    default:
                        return;
                }
            //}
            //else
            //{
            //    myProject.Save();
            //    myTiaPortal.Dispose();
            //    myTiaPortal = null;
            //    myProject = null;
            //    buttonConnectionTIA.Text = "Connect  \r\n to TIA Portal";
            //    msgWindow.AddMessage("Disconnect from TIA");
            //}

        }

        public void SearchProject()
        {

            OpenFileDialog fileSearch = new OpenFileDialog();

            fileSearch.Filter = "*.ap15_1|*.ap15_1";
            fileSearch.RestoreDirectory = true;
            fileSearch.ShowDialog();

            string ProjectPath = fileSearch.FileName.ToString();

            if (string.IsNullOrEmpty(ProjectPath) == false)
            {
                OpenProject(ProjectPath);
            }
        }

        public void OpenProject(string ProjectPath)
        {
            try
            {
                UserContainer.Project = objTiaPortal.Projects.Open(new FileInfo(ProjectPath));
                MessageBox.Show("Project " + ProjectPath + " opened");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while opening project" + ex.Message);
            }
        }

        public void SaveProject()
        {
            UserContainer.Project.Save();
            MessageBox.Show("Project saved");
        }
        public void CloseProject()
        {
            UserContainer.Project.Close();
        }




        public void AddHW()
        {
                            //6ES7 516 - 3AN01 - 0AB0
                //UserContainer.OrderNoGUI

            //btn_AddHW.Enabled = false;
            string MLFB = "OrderNumber:" + UserContainer.OrderNoGUI + "/" + UserContainer.VersionNoGUI;

            string name = UserContainer.DeviceNameGUI;
            string devname = "station" + UserContainer.DeviceNameGUI;
            bool found = false;
            foreach (Device device in UserContainer.Project.Devices)
            {
                DeviceItemComposition deviceItemAggregation = device.DeviceItems;
                foreach (DeviceItem deviceItem in deviceItemAggregation)
                {
                    if (deviceItem.Name == devname || device.Name == devname)
                    {
                        SoftwareContainer softwareContainer = deviceItem.GetService<SoftwareContainer>();
                        if (softwareContainer != null)
                        {
                            if (softwareContainer.Software is PlcSoftware)
                            {
                                PlcSoftware controllerTarget = softwareContainer.Software as PlcSoftware;
                                if (controllerTarget != null)
                                {
                                    found = true;

                                }
                            }
                            if (softwareContainer.Software is HmiTarget)
                            {
                                HmiTarget hmitarget = softwareContainer.Software as HmiTarget;
                                if (hmitarget != null)
                                {
                                    found = true;

                                }

                            }
                        }
                    }
                }
            }
            if (found == true)
            {
                UserContainer.StatusBarGUI = "Device " + UserContainer.DeviceNameGUI + " already exists";
            }
            else
            {
                Device deviceName = UserContainer.Project.Devices.CreateWithItem(MLFB, name, devname);

                UserContainer.StatusBarGUI = "Add Device Name: " + name + " with Order Number: " + UserContainer.OrderNoGUI + " and Firmware Version: " + UserContainer.VersionNoGUI;
            }

            

        }
    }

    



    //public void StartTiaPortal(bool withUI)
    //{
    //    try
    //    {
    //        if (withUI)
    //        {
    //            objTiaPortal = new TiaPortal(TiaPortalMode.WithUserInterface);
    //            this.MessageLogging("TIA Portal started with user interface.");
    //        }
    //        else
    //        {
    //            objTiaPortal = new TiaPortal(TiaPortalMode.WithoutUserInterface);
    //            this.MessageLogging("TIA Portal started without user interface.");
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    //public void DisConnectTiaPortal(TiaPortalProcess myProcess)
    //{
    //    try
    //    {
    //        if (myProcess == null) return;
    //        objTiaPortal = myProcess.Attach();
    //        if (objTiaPortal.GetCurrentProcess().Mode == TiaPortalMode.WithUserInterface)
    //        {
    //            _WithUI = true;
    //        }
    //        else
    //        {
    //            _WithUI = true;
    //        }


    //        if (objTiaPortal.Projects.Count <= 0)
    //        {
    //            throw new Exception("No TIA Portal Project was found!");
    //        }
    //        objTiaPortal.Dispose();
    //        this.MessageLogging(string.Format("TIA Portal disconnect: {0}", myProcess.ProjectPath.ToString()));
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    //public void ConnectTiaPortal(TiaPortalProcess myProcess)
    //{
    //    try
    //    {
    //        objTiaPortalProcess = myProcess;
    //        objTiaPortal = objTiaPortalProcess.Attach();
    //        if (objTiaPortal.GetCurrentProcess().Mode == TiaPortalMode.WithUserInterface)
    //        {
    //            _WithUI = true;
    //        }
    //        else
    //        {
    //            _WithUI = true;
    //        }
    //        if (objTiaPortal.Projects.Count <= 0)
    //        {
    //            throw new Exception("No TIA Portal Project was found!");
    //        }
    //        objProject = objTiaPortal.Projects[0];
    //        this.LoadTreeList();
    //        this.MessageLogging(objTiaPortalProcess.ProjectPath.ToString());
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}
}

