﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Siemens.Engineering;
using Siemens.Engineering.HW;
using Siemens.Engineering.HW.Features;
using Siemens.Engineering.SW;
using Siemens.Engineering.SW.Blocks;
using TIAProKonf.ImportFile;
using TIAProKonf.Database;

namespace TIAProKonf.WorkOnTia
{
    class FolderStructure
    {
        public static void StructCreate(Project myProject, MyExcelWorkSheet excelWorkSheet)
        {
            //Durch diese Methode wird die Ordnerstruktur aus der Excel-Tabelle im TIA Portal angelegt
            string value;
            excelWorkSheet.Infos.TryGetValue("IO-Controller", out value); //Namen des IO-Controllers aus der Excel-Tabelle
            Device device = myProject.Devices.Find(value); //Deviceobjekt des IO-Controllers holen
            PlcSoftware plcSoftware = device.DeviceItems[1].GetService<SoftwareContainer>().Software as PlcSoftware; //PLCSoftware des IO-Controllers holen
            PlcBlockUserGroupComposition groupComposition = null;
            PlcBlockUserGroup group = null;
            String path = null;

            //mit dieser For-Schleife werden alle Reihen der Excel-Tabelle durchlaufen und die Ordnerpfade abgefragt.
            for (int i = 1; i <= excelWorkSheet.DataRows; i++)
            {
                path = excelWorkSheet.GetData(i, "Pfad").ToString();
                groupComposition = plcSoftware.BlockGroup.Groups;
                //group = null;

                if (path.IndexOf("\\") == -1) // Wenn im Pfad kein \ vorhanden ist wird der Ordner in der obersten Ebene angelegt
                {
                    group = groupComposition.Find(path); // Wenn der Ordner nicht gefunden wurde wird er angelegt
                    if (group == null)
                    {
                        groupComposition.Create(path);
                    }
                }
                else
                {
                    groupComposition = findBlockGroups(groupComposition, path);
                    group = groupComposition.Find(path.Substring(path.LastIndexOf("\\") + 1));
                    if (group == null)
                    {
                        groupComposition.Create(path.Substring(path.LastIndexOf("\\") + 1));
                    }
                }
            }
        }

        private static PlcBlockUserGroupComposition findBlockGroups(PlcBlockUserGroupComposition groupComposition, string path)
        {
            //Diese Methode ermittelt den Ordner, welcher eine Ebene über den anzulegenden Ordner liegt
            PlcBlockUserGroupComposition getGroup;
            string search = path;
            if (path.IndexOf("\\") != -1)
            {
                search = path.Substring(0, path.IndexOf("\\"));
                path = path.Substring(path.IndexOf("\\") + 1);
            }
            foreach (PlcBlockUserGroup userGroup in groupComposition)
            {
                if (userGroup.Name.ToUpper().Equals(search.ToUpper()))
                {
                    if (path.IndexOf("\\") != -1)
                    {
                        getGroup = findBlockGroups(userGroup.Groups, path);
                        if (getGroup != null) return getGroup;
                    }
                    else return userGroup.Groups;
                }
            }
            return null;
        }

    }
}
